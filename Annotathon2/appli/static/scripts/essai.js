function doIt() {
  $( "span,div" ).show( "slow" );
}
// Can pass in function name
$( "button" ).click( doIt );

$( "form" ).submit(function( event ) {
  if ( $( "input" ).val() === "yes" ) {
    $( "p" ).show( 4000, function() {
      $( this ).text( "Ok, DONE! (now showing)" );
    });
  }
  $( "span,div" ).hide( "fast" );

  // Prevent form submission
  event.preventDefault();
});