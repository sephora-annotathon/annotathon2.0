
// Requires: utils,  notify

//utils.jsLoader.load(["../base/browser.js", "../base/debug.js"]);

//
// A RadioGroup simply shadows a group of radio buttons and emits events to the
// global dispatcher. Each time a radio button is selected, it emits a "desel" event
// for the old button, and a "sel" event for the new button. It also emits a "sel"
// event for the checked button when the page loads, and emits "desel" and "sel" events
// appropriately when the form is reset.
//
// Example
// For "node" to listen for "sel" and "desel" events on "target", and print the target''s
// id when it happens:
//
//   var n = Notifier.getNotifier();
//   n.setListener(node, "sel", function(obj, comment, msg, tgt) {
//      debug("Selected " + tgt.id);
//   }, target);
//

RadioGroup = function(name) {
   var elems = document.getElementsByName(name);
   var notifier = Notifier.getInstance();
   var oThis = this;
   oThis.checked = null;
   oThis.name = name;

   for (var i = 0; i < elems.length; i++) {
      var el = elems[i];
      if (el.tagName.toLowerCase() == 'input' &&
          el.getAttribute("type").toLowerCase() == 'radio') {
		 
         // Mark default as checked on load
         if (el.checked) {
            notifier.Notify(el, 'sel', el, null); // Tell everyone you were checked
            oThis.checked = el;            
         }
   
         utils.addEvent(el, 'click', function(e) { oThis.sync(); }, false);         
      }
   }
}

RadioGroup.prototype = {

   getChecked: function() {
       var elems = document.getElementsByName(this.name);
       var checkedElement = null;

       for (var i = 0; i < elems.length; i++) {
           var el = elems[i];
           checkedElement = el.checked ? el : checkedElement;
       }
       return checkedElement;
   },

   setChecked: function(elem) {
       elem.checked = true;
       this.sync();
   },

   sync: function() {
       var current = this.getChecked();
       
       if (this.checked && (this.checked != current)) {       		   
           var notifier = Notifier.getInstance();
           notifier.Notify(this.checked, 'desel', this.checked, null);
           notifier.Notify(current, 'sel', current, null);
       }
       this.checked = current;
   },

   // On reset, select the item that is marked "checked"
   onreset: function() {
       var radios = document.getElementsByName(this.name);
       var oThis = this;

       // Send reset event here? Not sure...

       for (var rr = 0; rr < radios.length; rr++) {
           var r = radios[rr];
           if (r.tagName.toLowerCase() == 'input' &&
               r.getAttribute("type").toLowerCase() == 'radio' &&
               r.defaultChecked) {
               r.checked = true;
               oThis.sync();
           }
       }
   }
}

RadioGroup.onload = function() {
   var nodes = jQuery("input[type='radio']");
   var names = new Object();
   var notifier = Notifier.getInstance();
   var oThis = this;

//   debug.enable();
   
   RadioGroup.groups = new Object();

   // Send debug messages
//   notifier.setListener(oThis, "*", function(obj, comment, msg, notif) {
//      debug(msg + ": " + comment.value + ", notif = " + (notif ? notif.toString() : "[null]"));
//   }, null);

   for (var n = 0; n < nodes.length; n++) {
       var node = nodes[n];
       var name = node.getAttribute("name");
       if (name && !RadioGroup.groups[name]) {
           RadioGroup.groups[name] = new RadioGroup(name);
           if (node.form) {
               utils.addEvent(node.form, "reset", function(e) {
                   RadioGroup.groups[name].onreset();
               }, false);
           }
       }
   }      
}

utils.addEvent(window, 'load', RadioGroup.onload, false);


