function UpdateLowComlexityFilter()
{
    if($("program").value == "blastp") {
        var lowCompFilter = false;
        if(this.selectedIndex == 0) {
            lowCompFilter = true;
        }
        SetFilterDefaults("L",lowCompFilter);	
    }
}

//**********SetProgTypeDefaults() and Co*******************************************************

function SetFilterDefaults(filterVal,checkFilter)
{
	filters = document.searchForm.FILTER;
	for(i=0; i < filters.length; i++) {
		if(filters[i].value == filterVal) {
			filters[i].checked = checkFilter;				
			diffFromDefault($(filters[i].id));
		}
	}
}


function SetProteinSuiteDefaults(program) //program = blastProgram here
{
    var runPsiBlast = (program == "blastp" || program == "kmerBlastp") ? "" : "on";
    var service = (program == "deltaBlast") ? "delta_blast" : "plain";
    
    compBasedList = "all";
    deltaThreshHold = ""; 
    psiThreshHold = "";
    numSeqOption = 2;

    $("runPSI").value = runPsiBlast;

    if ($("runPSI").value == "on") {                
        psiThreshHold = "0.005";
        numSeqOption = 4;        
    }
    changeOptionList($("wordsize"), program, true);
    diffFromDefault($("wordsize"));
    if (service == "delta_blast") {
        compBasedList = "delta";
        deltaThreshHold = "0.05";
    }    

    $("NUM_SEQ").options[numSeqOption].selected = true;
    $("NUM_SEQ").setAttribute("defVal", $("NUM_SEQ").options[numSeqOption].value);
    diffFromDefault($("NUM_SEQ"));

    changeOptionList($("compbasedstat"), compBasedList, true);
    diffFromDefault($("compbasedstat"));


    $("I_THRESH").value = psiThreshHold;
    if ($("DI_THRESH")) {
        $("DI_THRESH").value = deltaThreshHold;
        if ($("DI_THRESH").value != "") diffFromDefault($("DI_THRESH"));
    }

    //Check Low complexity Regions
    SetFilterDefaults("L", false);
    $("srv").value = service;
    if ($("selectedProg").value != "phiBlast") $("PHI_PATTERN").value = "";
    clearPSSM();
    if ($("pscount")) {
        $("pscount").value = 0;
        diffFromDefault($("pscount"));
    }    
}

function ShowHideCurrPrgmControls(selPrgm) {
    var showAttr = selPrgm.getAttribute("show");
    var show = (showAttr && showAttr.indexOf("phiPat") != -1) ? true : false;
    showHideElem("phiPat", !show); //show/hide //PHI pattern
}

function SetNucSuiteDefaults(program,setMegablast,page,defaultMatchScoresOption,filter,twoHits)
{
	
	$("runMegablast").value = setMegablast;		
	$("page").value = page;
					
	var setDefaults = true;
	var matchScoresIndex = 0;
	//Uncomment this block if want to show user's stcikcing options defaults when change the program
	/*if($("userDefaultprog").value != "" && $("userDefaultprog").value == program) {
	    setDefaults = false;
	    matchScoresOption = $("userDefaultMatchScores").value;
    }
    else {	        
        matchScoresOption = defaultMatchScoresOption;	
    }*/
    matchScoresOption = defaultMatchScoresOption;	
	
	changeOptionList($("wordsize"),program,setDefaults);	
	diffFromDefault($("wordsize"));
	//TO DO: Update that only if  megablast was selected before	
	$("matchscores").options[matchScoresOption].selected = true;
	//$("matchscores").defVal = $("matchscores").options[matchScoresOption].value;	
	$("matchscores").setAttribute("defVal",$("matchscores").options[defaultMatchScoresOption].value);		
	diffFromDefault($("matchscores"));
		
	updateGapcostNuc();					
	//Uncheck Mask for lookup tables
	//SetFilterDefaults("m",filter);
	
	//Set TWO_HITS for discoMegablast	
	$("twoHits").value = twoHits;
	$("templlength").setAttribute("defVal","18");	
	$("templlength").options[2].selected = true;	
	diffFromDefault($("templlength"));			
	diffFromDefault($("templtype"));		
	//Select 1,-2 for megablast
	initFiltersDefaults(true);	
}

function DisplayCurrProgram(program) {
    var selectedProg = $(program).getAttribute("progInfo");
    var selectedDescr = $(program).getAttribute("progDescr");    	    
    jQuery("[class='progInfo']").each(function(index) {    
        this.innerHTML = selectedProg;
    });            
    jQuery("[class='progDescr']").each(function(index) {    
        this.innerHTML = selectedDescr;
    });        
}

		
function SetPageDefaultParams(program) //program = blastProgram here
{
	
	//Set hidden field	
	$("selectedProg").value = program;	
	$("optSection").className =  program;
    if($("program").value == "blastp") {
        ShowHideCurrPrgmControls($(program));        
        SetProteinSuiteDefaults(program);
    }    		
	else if(program == "blastn") {
		//Set MEGABLAST.value = on,PAGE.value = "MegaBlast",Check Mask for lookup tables 
	    //Set WORD_SIZE list 7-15 sel= 11 GAPCOSTS 4,4-2,2 sel= 5,2,Match_scores 1,-2 - 1,-1 sel 2,-3,	    
		SetNucSuiteDefaults(program,"","Nucleotides",3,false,"");
	}
	else if(program == "megaBlast") {		
	    //Set MEGABLAST.value = "",PAGE.value = "Nucleotides",Uncheck Mask for lookup tables 
	    //Set WORD_SIZE list 16..64 sel = 28 GAPCOSTS = Linear - 1,1 sel=Linear,Match_scores 1,-2 - 1,-1 sel 1,-2,	    
		SetNucSuiteDefaults(program,"on","MegaBlast",0,false,"");
	}
	else if(program == "discoMegablast") {
		//Set MEGABLAST.value = on,PAGE.value = "MegaBlast",Check Mask for lookup tables 
	    //Set WORD_SIZE list 11,12 sel = 11 GAPCOSTS 4,4-2,2 sel = 5,2,Match_scores 1,-2 - 1,-1 sel=2,-3,	    
	    //Set TWO_HITS for discoMegablast
		SetNucSuiteDefaults(program,"on","MegaBlast",3,true,"on");			
	}
	else {	
		$("runPSI").value = "";
		$("runMegablast").value = "";
	}	
	DisplayCurrProgram(program);
}

function SetProgTypeDefaults()
{
    var blastProgram = this.value;                
    SetPageDefaultParams(blastProgram);    
}


function addOption(control,value,text)
{		
		//var anOption = document.createElement("OPTION") ;
		var anOption = new Option(value);
		
		if(text == "") {
			text = value;
		}
		anOption.text = text;
		anOption.value = value;
        if(navigator.userAgent.match(/safari/i)) {		
		    control.add(anOption);
		}
		else {
		    control.options.add(anOption);
		}
		//anOption.selected = true;
		return anOption;
}

function createOptionList(optionListName, arrayofOptions)
{
	optionListName.options.length = 0;
	for(i=0; i < arrayofOptions.length; i++) {
		anOption = addOption(optionListName,arrayofOptions[i++],"");		
		if(arrayofOptions[i] == "selected") {
			anOption.selected = true;
		}		
	}	
}


//hiddenField.value = optionListName.innerHTML; <option>djdjfskd</option><option>
//Save original option list data in div with id="savedLists"
//Saved option list id= optionList.name + "_ID
function saveOriginalOptionList(optionList) 
{
	div = $("savedLists");
	div.innerHTML = div.innerHTML + "<select id='" + optionList.name + "_ID'>" + optionList.innerHTML + 		
					"</select>"
}

//This function finds <select> list with id = optionList.id + matchName on the page 
//which is hidden inside div with id =savedLists. This list contains all original <select> WORDSIZE, GAPCOST, etc lists
//optionList is substituted by the new list 
//If item has class="Deflt" this item will be selected if setDefaults)=true
//If item has class only_progname - it wil be added in the new list only for this progname
function changeOptionList(optionList,matchName,setDefaults)
{
    //For gapcosts (example 1,-4) remove comma since it is not permitted in ID     
    matchName  = matchName.replace(/,/g,"");    
	savedListID = optionList.id + "_" + matchName;
	var savedList = $(savedListID);
	var defValue = ""; var j = 0;
	optionList.options.length = 0;
	var indexSelected = -1;
	var indexDefault = -1;;
	
	for(i=0; i < savedList.options.length; i++) {
	    //if there is only megaBlast in class name - add this option only for megablast    
	    if(utils.hasClass(savedList.options[i],"only")) {
            //utils.hasClass(savedList.options[i],"only_megaBlast")
	        if(utils.hasClass(savedList.options[i],$("selectedProg").value)) {
	            optionList.setAttribute("defVal","");
	            indexDefault = 0;	        
	        }
	        else continue;                        	    
	    }
	    
		var currOpt = addOption(optionList,savedList.options[i].value,savedList.options[i].text);
		if(optionList.multiple == true)  currOpt.selected = savedList.options[i].selected;
		       
        if(!setDefaults) {
            if(savedList.options[i].selected) indexSelected = j;  
		}		
		else {
		    if(utils.hasClass(savedList.options[i],"Deflt")  && indexDefault == -1) indexSelected = j;
        }		    
        
		if(utils.hasClass(savedList.options[i],"Deflt") && indexDefault == -1) {		
		    optionList.setAttribute("defVal",savedList.options[i].value);
		    indexDefault = j;
		}		
		j++;		
	}
	if(indexDefault== -1) indexDefault = 0;
	if(indexSelected == -1) indexSelected = indexDefault;
	optionList.options[indexSelected].selected = true;	
}

function updateGapcostNuc()
{
    var setDefaults = true;
    //Uncomment this line if want to show user's stcikcing options defaults when change the program
	//if($("userDefaultprog").value == $("selectedProg").value ) setDefaults = false;
    updateGapcostNucDef(setDefaults);	
}

function updateGapcostProt()
{
    var setDefaults = true;
    //Uncomment this line if want to show user's stcikcing options defaults when change the program
	//if($("userDefaultMatrix").value ==  $("matrixName").selectedIndex) setDefaults = false;
    updateGapcostProtDef(setDefaults);	
}

function updateGapcostNucDef(setDefaults)
{
	list = $("matchscores");  
	if($("selectedProg").value == "megaBlast") {		
		//setDefaults = false;//will default to the first item in the list which is Linear
	}
	changeOptionList($("gapcosts"),
					 list[list.selectedIndex].value,					 
					 setDefaults);    
    diffFromDefault($("gapcosts"));					 
}

function updateGapcostProtDef(setDefaults)
{
	list = $("matrixName");  	
	if (list) changeOptionList($("gapcosts"),
					 list[list.selectedIndex].value,					 
					 setDefaults);	
    //Gapcosts will always show as non-default, if matrix is non-default					     
    //if(utils.hasClass($("matrixName_def"),"diff")) {
    /*if(utils.hasClass(utils.getParent($("matrixName")),"nondef")) {    
        $("gapcosts").defVal = "0";        
    }*/					 
    diffFromDefault($("gapcosts"));
    if($("selectedProg").value != "blastx" && $("selectedProg").value != "tblastx") {					 	    
	    if ($("compbasedstat")) diffFromDefault($("compbasedstat"));						 
	}
}

function setDefaultVals(fieldsType) {
    jQuery(fieldsType).each(function (index) {
        var par = utils.getParent(this);
        //alert(defValNodes[i].id + " " + par.className + " " + $("selectedProg").value);
        if (utils.hasClass(par, "all") || utils.hasClass(par, $("selectedProg").value)) {
            setDefalValue(this);
            //alert(defValNodes[i].id + " " + par.className + " " + $("selectedProg").value);	    
        }
    });
    if ($("page").value == "Proteins" || $("page").value == "Translations") updateGapcostProt()
    else updateGapcostNuc();
    $("diffMes").style.display = "none";
    $("NUM_DIFFS").value = 0;
    $("NUM_OPTS_DIFFS").value = 0;
}


function setAllDefaultVals() 
{
    setDefaultVals(".reset");    
}

function ResetAlgParams(e) {
    setDefaultVals("#moreopts .reset");
    utils.preventDefault(e);    
}	


function ResetGen()
{
    RadioGroup.groups["BLAST_PROGRAMS"].onreset();    
    //First reset current program defaults    
    if($("defaultProg") != null) {//This is set for blastn and blastp
        blastProgram = $("defaultProg").value;
    }
    else {       
        blastProgram = $("program").value;            
    }    
    if($(blastProgram)) {                
        $(blastProgram).checked = true;
        SetPageDefaultParams(blastProgram);        
    }    
    if($("bl2seq")) {
        $("bl2seq").checked = false;
        utils.removeClass($("type-a"), "blast2seq");
        $("blastSpec").value = "";  
    }
    $("savedSearch").value = "false";
    setAllDefaultVals();
    setupDBDisplayParams();
    resetOrganismControls($("searchForm").EQ_MENU);
    if ($("orgDbs")) $("orgDbs").value == "";
    //The next line does not work in Safari for some reason
    //$("qorganism").value = "";    
    $("userDefaultprog").value = "";
    if($("userDefaultMatrix")) $("userDefaultMatrix").value = "";        
    jQuery(".msg").each(function(index) {    
        utils.removeClass(this,"error");
    });        
    document.searchForm.action = "Blast.cgi";
    if($("subgroup") && $("DATABASE").type == "select-one") {	    
	    ShowDbSubGroup(false);	    
	}		
}


function ResetForm()
{
    if ($("blastInit") && $("blastInit").value == "blast2seq" || ($("specBlast") && $("specBlast").value == "on")) {    
        ResetSpec();
    }
    else {
        ResetGen();
    }
}

function ResetSpec()
{
    var url = "Blast.cgi?PROGRAM=" + $("program").value + "&PAGE_TYPE=BlastSearch&PROG_DEFAULTS=on";
    if ($("blastInit")) {
        url += "&BLAST_INIT=" + $("blastInit").value;
    }
    url += "&BLAST_SPEC=" + $("blastSpec").value;
    if ($("blastSpec").value == "GlobalAln") {
        url += "&BLAST_PROGRAMS=" + $("program").value;
    }
                
    location.href = url;
}

function linksInit() {
    jQuery("[class='resetlink']").each(function(index) {
        utils.addEvent(this, "click", ResetForm, false);
    });
    jQuery("[name='getURL']").each(function(index) {
        utils.addEvent(this, "click", GetURL, false);
    });
    clearQueryInit();

    jQuery("[class='progLinks']").each(function(index) {
        utils.addEvent(this, "click", InitLogParams, false);
    });
    jQuery("[class='resetOpts']").each(function (index) {
        utils.addEvent(this, "click", ResetAlgParams, false);
    });
}   

function clearQuery() {
    var f = this.getAttribute("fieldToClear");    
    if(f) {
        if ($("blastSpec").value == "GlobalAln") {
            resetInputHint("#" + f);
        }
        else {
            jQuery("#" + f).attr("value","");
        }
    }
}

function clearQueryInit() {   
   jQuery("[class='clearlink']").each(function(index) {   
      utils.addEvent(this, "click", clearQuery, false);
   });
}



function clearPSSM()
{
    var pssmCtrl = $("promptPSSM");    
    utils.addClass(pssmCtrl, "hide");
    diffFromDefault($("savedPSSM"));
    
    pssmCtrl = $("uplPSSM");    
    if(utils.hasClass(pssmCtrl, "hide")) {
        utils.removeClass(pssmCtrl, "hide");
    }
    $("dpssm").value = "";                
}

function setupPssm()
{
   var idForPssm = $("dpssm"); //Hidden fiedl tha contains rid or Cubby ID for blastObj to get pssm
   if(idForPssm && $("promptPSSM") && $("uplPSSM")) {
      if(idForPssm.value == "") {
        utils.addClass($("promptPSSM"), "hide");        
      }
      else {
        utils.addClass($("uplPSSM"), "hide");
      }
   }    
   
   var clPssmm = $("cpssm");
   if (clPssmm) {
      utils.addEvent(clPssmm, "click", clearPSSM, false);
   }      
}

function setupDiffEvents()
{
    if ($("DATABASE")) utils.addEvent($("DATABASE"), "change", function() { diffFromDefault($("DATABASE"));  }, false); 
    if ($("NUM_SEQ")) utils.addEvent($("NUM_SEQ"), "change", function() { diffFromDefault($("NUM_SEQ"));  }, false); 
    if ($("expect")) utils.addEvent($("expect"), "change", function() { diffFromDefault($("expect"));  }, false); 
    if ($("wordsize")) utils.addEvent($("wordsize"), "change", function() { diffFromDefault($("wordsize"));  }, false); 
    if($("matchscores")) {
        utils.addEvent($("matchscores"), "change", function() { diffFromDefault($("matchscores"));  }, false);
    }     
    if ($("gapcosts")) utils.addEvent($("gapcosts"), "change", function() { diffFromDefault($("gapcosts"));  }, false);
    if ($("matrixName")) utils.addEvent($("matrixName"), "change", function() { diffFromDefault($("matrixName"));  }, false);    
    if ($("compbasedstat")) utils.addEvent($("compbasedstat"), "change", function() { diffFromDefault($("compbasedstat"));  }, false);        
    if ($("fil_l")) utils.addEvent($("fil_l"), "click", function() { diffFromDefault($("fil_l"));  }, false); 	
    if ($("fil_m")) utils.addEvent($("fil_m"), "click", function() { diffFromDefault($("fil_m"));  }, false); 	
    if ($("fil_r")) utils.addEvent($("fil_r"), "click", function() { diffFromDefault($("fil_r"));  }, false); 	
    if ($("lcm"))utils.addEvent($("lcm"), "click", function() { diffFromDefault($("lcm"));  }, false); 	    
    if ($("templlength"))utils.addEvent($("templlength"), "change", function() { diffFromDefault($("templlength"));  }, false);            
    if ($("templtype")) utils.addEvent($("templtype"), "change", function() { diffFromDefault($("templtype"));  }, false);            
    if ($("pscount")) utils.addEvent($("pscount"), "change", function() { diffFromDefault($("pscount"));  }, false);
    if ($("hsp_range_max")) utils.addEvent($("hsp_range_max"), "change", function() { diffFromDefault($("hsp_range_max")); }, false);
    if ($("DI_THRESH")) utils.addEvent($("DI_THRESH"), "change", function() { diffFromDefault($("DI_THRESH")); }, false);
}

function checkAll(className,check)
{
    jQuery("[class='" + className + "']").each(function(index) {      
      this.checked = check;      
    });    
}


function iniSRADB() 
{
    
    $('blastType').value = "SRA";
    sraExpInp = false;    
    var suggestHint = $("suggestHint") ? $("suggestHint").value : $("qorganism").getAttribute("suggestHint");
    if ($("qorganism").value != "" && $("qorganism").value != suggestHint) {
        sraExpInp = true;
    }    
    if (!sraExpInp) {
        var multiOrg = jQuery(".multiOrg"); 
        for (var i = 0; i < multiOrg.length; i++) {
            if ($(multiOrg[i]).value != "" && $(multiOrg[i]).value != suggestHint) {
                sraExpInp = true;
                break;
            }
        }
    }    
    if (!sraExpInp) {
        alert("Please enter an SRX experiment under 'Choose Search Set'");
    }    
    return sraExpInp;
}

function adjustDatabaseValue() 
{
    var dbelem = $("DATABASE");
    var adjsutDbVal = false;
    if ($("bl2seq") && $("bl2seq").checked == true) {
        dbVal = "";
        adjsutDbVal = true;
    }
    else if ($("seqFromType") && $("seqFromType").checked == true) {
        dbVal = $("seqFromType").getAttribute("database");
        if(dbVal && dbVal != "") adjsutDbVal = true;
    }
    if (adjsutDbVal && dbelem) {
        if (dbelem.type == "select-one") {
            var selectedIndex = getSelectedDbIndex();
            $("DATABASE")[selectedIndex].value = dbVal;
        }
        else {
            $("DATABASE").value = dbVal;
        }
    }
}

function isSRA() 
{
    var sraSearch = $("blastSpec").value == "SRA" || $('blastType').value == "SRA";
    return sraSearch;
}

var g_DefaultOrgSuggestMsg = "Enter organism name or id--completions will be suggested";

function checkOrgInclusion(organism) 
{
    var orgInfo = new Object();
    orgInfo.id = $(organism).id;
    orgInfo.include = false;
    orgInfo.exclude = false;

    var exclid = "orgExcl" + $(organism).id.replace("qorganism", "");
    if ($(organism).value != "" && $(organism).value != g_DefaultOrgSuggestMsg) {
        if ($(exclid).checked) {
            orgInfo.exclude = true;
        }
        else {
            orgInfo.include = true;
        }
    }
    return orgInfo;
}


function isOrgExcludeOnly() 
{
    var hasExclude = false;
    var hasInclude = false;

    orgInfo = checkOrgInclusion($("qorganism"));    
    if(orgInfo.exclude) hasExclude = true;
    if (orgInfo.include) hasInclude = true;

    jQuery(".multiOrg").each(function (index) {
        orgInfo = checkOrgInclusion(this);
        if (orgInfo.include) hasInclude = true;
        if (orgInfo.exclude) hasExclude = true;        
    });
    
    var exclOnly = !hasInclude && hasExclude;        
    return exclOnly;
}

function setBlastType()
{
    if($("DATABASE").type == "select-one") {
        var selectedIndex = getSelectedDbIndex();
        var dbOption = $("DATABASE")[selectedIndex];
    }
    else {
        dbOption = $("DATABASE");
    }
    
    var blastType = dbOption.getAttribute("blasttype");
    if ($("blastType")) {
        $("blastType").value = (blastType != null) ? blastType : "";
    }
}


function initSubmit()
{
    if (navigator.userAgent.match(/safari/i)) {
        var referrer = jQuery("meta[name=referrer]").attr("content");        
        jQuery("meta[name=referrer]").attr("content", "origin");
    }
    
    jQuery("[name='NEWWIN']").each(function(index) {            
        if (this.checked) $("searchForm").target = "Blast_Results_for_" + Math.floor(Math.pow(10, 10) * Math.random());
    });    
    adjustOrgVal($("searchForm").EQ_MENU);
    adjustDatabaseValue();
    setBlastType();
    if ($("blastSpec").value.indexOf("MicrobialGenome") != -1 && ($("program").value == "blastp" || $("program").value == "blastx")) {
        if(!isOrgExcludeOnly()) $("entrzPrs").value = "";//reset ENTREZ_PRESET if not exclusive organism exclude
    }
    if ($("wpProteins")) setIncludeEntrezQuery($("wpProteins"));
    if ($("wgsLimitBy")) {
        var valid = validateWGSInput();
        if (!valid) return false;
    }
    if ($("bl2seq") && !$("bl2seq").checked && $("subj")) $("subj").value = "";
    processOrgDbs();
    if (isSRA()) {
        if (!iniSRADB()) return false;           
        if (checkSRASearchSetSize()) return false;        
    }
    if ($("srv").value == "delta_blast" && multipleQueries("seq")) {
        alert("DELTA-BLAST is not supported for multiple sequences");
        $("seq").focus();
        return false;
    }
    else if ($("blastSpec").value.indexOf("MicrobialGenome") != -1 && $("orgDbs") && $("orgDbs").value != "") {
        if (!validateRequiredOrganism()) {
            alert("Please, enter organism");
            return false;
        }
    }
    else if ($("qorganism") && $("qorganism").value != "") {
        var dictType = $("qorganism").getAttribute("dictType");
        if(!dictType || dictType == "taxid") {
            if (!validateOrganismTaxid()) return false;
        }
    }
    jQuery("[class='blastbutton']").each(function(index) {           
        if(!$("nw1").checked) {
            jQuery(this).addClass("dsbl"); 
            jQuery(this).attr("disabled", "true");       
        }
    });            
    return true;
}


function SetEvents(e)
{
   jQuery("[name='BLAST_PROGRAMS']").each(function(index) {           
        utils.addEvent(this, "click", SetProgTypeDefaults, false);           
   });      
   var list = $("matrixName");
   if (list) {
      utils.addEvent(list, "change", updateGapcostProt, false);
   }
   list = $("matchscores");
   if (list) {
      utils.addEvent(list, "change", updateGapcostNuc, false);
   }   
   list = $("compbasedstat");
   if (list) {
      utils.addEvent(list, "change", UpdateLowComlexityFilter, false);
   }
   utils.addEvent($("searchForm"), "submit", initSubmit, false);
   
   jQuery("[class='blastbutton']").each(function(index) {           
        var link = utils.getParent(this);     
        utils.addEvent(link, "click", function(e) {
                    e = e || window.event;
                    utils.preventDefault(e); 
                    if(initSubmit())
                    $("searchForm").submit();}, false);          
        InitCustomButton(this);
   });   
   InitCustomButton($("addOrgIm"));

   jQuery("[class='newwin']").each(function(index) {    
      utils.addEvent(this, "click", function() { checkAll("newwin", this.checked); }, false);
  });
    
   
   setupPssm();
   linksInit();   
   setupDiffEvents();
   new AccnLookup($('seq'), $('qtitle'), $('db')); 
   if($("bl2seq")) {
    utils.addEvent($("bl2seq"), "click", bl2SeqInit, false);
   }
   else if($("blastSpec").value == "blast2seq") {
   //remove this after adding bl2seq
        $("blastSpec").value = "";
    }
   
   jQuery("[class='dbgroup']").each(function(index) {       
        utils.addEvent(this, "click", dbGroupInit, false);
   });    
   
   jQuery("[class='wgsLimitBy']").each(function(index) {   
       utils.addEvent(this, "click", wgsGroupInit, false);
   });    
}

function checkFormDefaultDiff()
{
	var count = 0;
    jQuery(".checkDef").each(function(index) {	    	
	  var par = utils.getParent(this);	  
	  //if(par.HasclassName.indexOf("all") != -1 || par.className.indexOf($("selectedProg").value) != -1) {	  	    
	  if(utils.hasClass(par,"all") || utils.hasClass(par,$("selectedProg").value)) { 	    
	    count += diffFromDefault(this); 
	    //alert(defValNodes[i].id + " " + par.className + " " + $("selectedProg").value + "count=" + count);
	  }    });    
	if(count > 0) {
	    $("diffMes").style.display = "inline";
	    $("NUM_DIFFS").value = count; 
	}	
}	

//if($("program").value != "blastn" || $("blastSpec").value != ""){
function HasGenomeNucDatabases() 
{
    var ret = false;    
    if ($("program").value == "blastn" && ($("blastSpec").value == "" || $("blastSpec").value == "blast2seq")) {        
        ret = true;
    }
    return ret;
}


function resetOrganism() 
{    
    if($("blastSpec").value == "") {
	if ($("searchForm").EQ_MENU) resetOrganismSuggest($("searchForm").EQ_MENU);
    }
}

function blastSurvey() {
    (function (t, e, s, o) { var n, a, c; t.SMCX = t.SMCX || [], e.getElementById(o) || (n = e.getElementsByTagName(s), a = n[n.length - 1], c = e.createElement(s), c.type = "text/javascript", c.async = !0, c.id = o, c.src = ["https:" === location.protocol ? "https://" : "http://", "widget.surveymonkey.com/collect/website/js/tRaiETqnLgj758hTBazgdwINbe8_2FWhBurBaqpnZstlMfy7ruPTOWEBdo5PE9rCfd.js"].join(""), a.parentNode.insertBefore(c, a)) })(window, document, "script", "smcx-sdk");
}        
        

utils.addEvent(window,"load", SetEvents,false);

var load = true;
function OnLoadHandler() {	
    // Set up database radios and dropdown to update each other
    setupDBDisplayParams();
    if ($("searchForm").EQ_MENU) setupOrganismSuggest($("searchForm").EQ_MENU);    
    // Hook up radio buttons to select the appropriate thing        
    //if($("program").value == "blastn" && ($("blastSpec").value == "" || $("blastSpec").value == "blast2seq")){
    if (HasGenomeNucDatabases()) {        
            jQuery("#genomeDbs").find("input[type='radio']").each(function (index) {
                utils.addEvent(this, 'click', function () {setDbDefaults(this, true);}, false);
                utils.addEvent(this, 'click', function () { ncbi.sg.ping(this, "click");}, false);
            });
                        
            // Automatically check the appropriate radiobutton when datbase is seleceted
            // radio button is clicked.
            utils.addEvent($('DATABASE'), 'change', function() {
                autoCheck(this.selectedIndex);
            }, false);            
    }
    else if ($("dbHelp") && $("dbHelp").getAttribute("getDbInfo") && $("dbHelp").getAttribute("getDbInfo") != "off") {
        utils.addEvent($("DATABASE"), "change", showDbInfoChange, false);
    }
        
    var mcr = jQuery("[class='microb']");    
    for (var i = 0; i < mcr.length; i++) {        
        utils.addEvent(mcr[i], "click", function() { displayCurrOrg(true); }, false);
    }
    initDBStatInfo();
    if (mcr.length > 0) {
        if ($("subgroup")) utils.addEvent($("subgroup"), "change", function() { displayCurrOrg(false); }, false);        
        //initDBStatInfo();
        displayCurrOrg(true);            
    }
    if (($("blastSpec").value == "SRA" && !RadioGroup.groups["DB_GROUP"]) || $("blastType").value == "SRA") {        
        displayDBStatInfo();
    }
    utils.addEvent($("DATABASE"), "change", DisplayCurrDatabase, false);
    utils.addEvent($("DATABASE"), "change", setupDynamicFilter, false);
    if ($("dbHelp") && $("dbHelp").getAttribute("getDbInfo") && $("dbHelp").getAttribute("getDbInfo") != "off") {
        utils.addEvent($("dbHelp"), "click", showDbInfoInit, false);
    }
    utils.addEvent($("DATABASE"), "change", setupDbDynParams, false);
    
    

    utils.addEvent($("seq"), "blur", checkQuerySubrange, false);
    utils.addEvent($("subj"), "blur", checkSubjSubrange, false);
    utils.addEvent($("QUERY_FROM"), "keyup", checkQuerySubrange, false);
    utils.addEvent($("QUERY_TO"), "keyup", checkQuerySubrange, false);
    if ($("qorganism")) {
        jQuery("#qorganism").bind("blur", function (e) { resetTopOption(e); });
        jQuery("#qorganism").bind("ncbiautocompletechange", function (e) { resetTopOption(e); });
    }

    if ($("SUBJECTS_FROM")) utils.addEvent($("SUBJECTS_FROM"), "keyup", checkSubjSubrange, false);
    if ($("SUBJECTS_TO")) utils.addEvent($("SUBJECTS_TO"), "keyup", checkSubjSubrange, false);
    
	if($("page").value == "Proteins" || $("page").value == "Translations") updateGapcostProtDef(false) 
	else  updateGapcostNucDef(false);	        
	
	if($("program").value == "blastn") $("userDefaultMatchScores").value = $("matchscores").selectedIndex;
	if($("program").value == "blastp" && $("matrixName")) $("userDefaultMatrix").value = $("matrixName").selectedIndex;
	
	//initRepeatFilter(); //only for organism pages
	checkFormDefaultDiff();
	//alert($("NUM_DIFFS").value);
	if ($("NUM_OPTS_DIFFS").value != "0") {
	    jQuery($("algPar")).ncbitoggler('toggle');	    
	}
	ValidateUserParams();
	if ($("addOrg")) utils.addEvent($("addOrg"), "click", AddOrgField, false);	    
	
	if ($("subgroup") && $("DATABASE").type == "select-one") {	    
	    ShowDbSubGroup(false);	    
	    utils.addEvent($("DATABASE"), "change", function() {      
            ShowDbSubGroup(true);       
        }, false);
    }
    if ($("blastSpec").value != "blast2seq") {
        if ($("dbHelp") && $("dbHelp").getAttribute("getDbInfo") && $("dbHelp").getAttribute("getDbInfo") != "off") {
            showDbDetails();
        }
    }
    if($("blastSpec").value == "GlobalAln") {
        setupInputHint("#seq");
        setupInputHint("#subj");
        jQuery("#upl").bind("click", function () { removeInputHint("#seq"); });
        jQuery("#s_upl").bind("click", function () { removeInputHint("#subj"); });        
    }
    load = false;
}
     

utils.addEvent(window, 'load', OnLoadHandler, false);

signInMyNCBI = function() {
   for (var i = 0; i < document.links.length; i++) {
      var l = document.links[i];
      var h = l.getAttribute("href");
      var t;
      if (l.textContent) {
        t = l.textContent;
      } else {          
        t = l.innerText;
      }
      if (t) { t = t.toLowerCase(); }
//      var t = ((l.textContent>'') ? l.textContent : l.innerText).toLowerCase();
      // Find link to JS
      if (h && t == "[sign in]" && h.search('login') > 0) {
          if (document.createEvent) {
             var e = document.createEvent("MouseEvents");
             e.initEvent("click", true, false);
             l.dispatchEvent(e); 
          } else {
             l.click();
          }
      }
   }
   return false;
}

function getUrlCompForCheckedField(elem)
{
    var url = "";
    if (elem && elem.checked) {		    
	    url = "&" + elem.name + "=" + elem.value;
	}
	return url;
	
}

function getUrlCompForRadioCheckboxField(elementArr)
{
    var url = "";
    for(i = 0; i < elementArr.length; i++) {	    
        url += getUrlCompForCheckedField(elementArr[i]);	    
	}
	return url;
	
}


function getUrlCompForEntryField(ID)
{
    var url = "";
    if($(ID) && $(ID).value != "") {
        url = "&" + $(ID).name + "=" + escape($(ID).value);
    }
    return url;
}

function getUrlCompForOptionsField(ID) 
{
    var url = "";
    if ($(ID)) {
        url = "&" + $(ID).name + "=" + $(ID)[$(ID).selectedIndex].value;
    }
    return url;
}

function getUrlCompForMultiOptionsField(ID) {
    var url = "";
    if ($(ID)) {
        for (i = 0; i < $(ID).options.length; i++) {
            if ($(ID).options[i].selected) {
                url += "&" + $(ID).name + "=" + $(ID).options[i].value;
            }
        }
    }
    return url;
}

function getUrlForFormattingOptions() {  
    var url = getUrlCompForEntryField("SHOW_OVERVIEW");
    url += getUrlCompForEntryField("SHOW_LINKOUT");
    url += getUrlCompForEntryField("ALIGNMENT_VIEW");
    url += getUrlCompForEntryField("MASK_CHAR");
    url += getUrlCompForEntryField("MASK_COLOR");    
    url += getUrlCompForEntryField("GET_SEQUENCE");        
    url += getUrlCompForEntryField("NEW_VIEW"); 
    url += getUrlCompForEntryField("NCBI_GI");
    url += getUrlCompForEntryField("FORMAT_ORGANISM"); 
    url += getUrlCompForEntryField("FORMAT_EQ_TEXT");     
    url += getUrlCompForEntryField("NUM_OVERVIEW"); 
    url += getUrlCompForEntryField("DESCRIPTIONS");
    url += getUrlCompForEntryField("ALIGNMENTS"); 
    url += getUrlCompForEntryField("FORMAT_OBJECT");
    url += getUrlCompForEntryField("FORMAT_TYPE");
    url += getUrlCompForEntryField("SHOW_CDS_FEATURE");    
    url += getUrlCompForEntryField("EXPECT_HIGH"); 
    url += getUrlCompForEntryField("EXPECT_LOW");     
    //QUERY_INDEX
    return url;    
}
    
function GetURLGen()
{
    url = getUrlCompForEntryField("qquery");//EQ_TEXT
    if($("DATABASE").type == "select-one") {
        url += getUrlCompForOptionsField("DATABASE");//DATABASE
    }
    else if($("DATABASE").type == "hidden") {
       url += getUrlCompForEntryField("DATABASE");//DATABASE
    }
    url += getUrlCompForRadioCheckboxField( $("searchForm").BLAST_PROGRAMS);    
    url += getUrlCompForOptionsField("NUM_SEQ");//MAX_NUM_SEQ
    url += getUrlCompForCheckedField($("adjparam"));//SHORT_QUERY_ADJUST
    url += getUrlCompForEntryField("expect");//EXPECT
    url += getUrlCompForOptionsField("wordsize");//WORD_SIZE
    if($("program").value == "blastn") {        
        url += getUrlCompForOptionsField("repeats");//REPEATS
        url += getUrlCompForOptionsField("templtype");//TEMPLATE_TYPE
        url += getUrlCompForOptionsField("templlength");//TEMPLATE_LENGTH
    }
    else if($("program").value != "blastn") {
        url += getUrlCompForOptionsField("matrixName");//MATRIX_NAME   
        url += getUrlCompForOptionsField("compbasedstat");//COMPOSITION_BASED_STATISTICS  
    }
    else if($("program").value == "tblastx" || $("program").value == "blastx") {
        url += getUrlCompForOptionsField("GENETIC_CODE");//GENETIC_CODE
    }
    var blastProg = $("selectedProg").value;    
    if(blastProg == "phiBlast") {     
        url += getUrlCompForEntryField("PHI_PATTERN");//PHI_PATTERN
    }
    if (blastProg == "phiBlast" || blastProg == "psiBlast" || blastProg == "deltaBlast") {
        url += getUrlCompForEntryField("I_THRESH");//I_THRESH
    }
    
    url += getUrlCompForRadioCheckboxField($("searchForm").FILTER);
    url += getUrlCompForCheckedField($("lcm"));//LCASE_MASK
    //PSSM??
    if($("program").value == "blastn" || blastProg == "discoMegablast" || blastProg == "megaBlast") {
        url += getUrlCompForEntryField("blastType");//WWW_BLAST_TYPE
    }
    url += GetOrganismURL();
    return url;
}

function GetOrganismURL() {
    var defaultMessage = ($("suggestHint")) ? $("suggestHint").value : g_DefaultOrgSuggestMsg;
    var suggestHint = $("qorganism").getAttribute("suggestHint");
    if (suggestHint) defaultMessage = suggestHint;

    var url = getUrlCompForEntryField("qorganism"); //EQ_MENU
    url += getUrlCompForCheckedField($("orgExcl"));    
    var numOrg = 1;    
    jQuery(".multiOrg").each(function(index) {
        if ($(this).value != defaultMessage) {
            url += "&EQ_MENU" + numOrg + "=" + $(this).value;
            var exclId = "orgExcl" + $(this).id.replace("qorganism", "");
            if ($(exclId) && $(exclId).checked) {
                url += "&ORG_EXCLUDE" + numOrg + "=on";
            }
            numOrg++;
        }
    });        
    if (numOrg > 1) url += "&NUM_ORG=" + numOrg;
    return url;
}
  
function GetURL ()
{

    var getUrl = "Blast.cgi?PAGE_TYPE=BlastSearch&USER_FORMAT_DEFAULTS=on&SET_SAVED_SEARCH=true";
    
    getUrl += getUrlCompForEntryField("page");//PAGE
    getUrl += getUrlCompForEntryField("program");//PROGRAM

    getUrl += getUrlCompForEntryField("seq"); //QUERY
    getUrl += getUrlCompForEntryField("subj"); //SUBJECT
    getUrl += getUrlCompForEntryField("QUERY_TO");
    getUrl += getUrlCompForEntryField("QUERY_FROM");        
    getUrl += getUrlCompForEntryField("qtitle");//JOB_TITLE  
    
    getUrl += getUrlCompForCheckedField($("nw1"));//NEWWIN
    getUrl += getUrlCompForCheckedField($("nw2"));//NEWWIN
    
    getUrl += getUrlCompForOptionsField("gapcosts");//GAPCOSTS        
    if($("program").value == "blastn") {
        getUrl += getUrlCompForOptionsField("matchscores");//MATCH_SCORES 
        
    }           
    getUrl += getUrlCompForEntryField("blastSpec");//BLAST_SPEC
    if($("blastSpec").value != "GlobalAln") {
        getUrl += GetURLGen();
    }
    else {        
        getUrl += getUrlCompForEntryField("subj");//SUBJECTS        
        getUrl += getUrlCompForEntryField("SUBJECTS_FROM");//SUBJECTS        
        getUrl += getUrlCompForEntryField("SUBJECTS_TO");//SUBJECTS                
    }
    //&DB_GROUP=AllMG&SUBGROUP_NAME=All_genomes&DB_SUBGROUP=Complete_Genomes&&DB_SUBGROUP=Complete_Plasmids&&DB_SUBGROUP=wgs
    if (RadioGroup.groups["DB_GROUP"]) {
        getUrl += getUrlCompForRadioCheckboxField($("searchForm").DB_GROUP);          
    }
    if($("sbName") &&  $("sbName").value != "") {
        getUrl += getUrlCompForEntryField("sbName");//SUBGROUP_NAME
        getUrl += getUrlCompForMultiOptionsField("subgroup");
    }
    
    var blastProg = $("selectedProg").value;
    if (blastProg == "tblastx" || blastProg == "tblastn" || blastProg == "blastx" || $("blastSpec").value == "GlobalAln") {
        getUrl += "&BLAST_PROGRAMS=" + blastProg;
    }
    else {
        getUrl += "&PROG_DEFAULTS=on";
    }
    getUrl += getUrlForFormattingOptions();
    // WB-808
    if ( (undefined != location.href.match(/\?SV_SHOW\=TRUE|&SV_SHOW\=TRUE/i)) || 
         ($("SV_SHOW") && $("SV_SHOW").value == "true")
        ) {
        getUrl += "&SV_SHOW=TRUE";
    }    
    alert("Please bookmark the next page in your browser");
    location.href = getUrl;                                           
}


function hideOrganisms(hide)
{
    if (!utils.hasClass($('orgInfo'), "fixed")) {
        if (hide) {
            utils.addClass($('orgInfo'), "hidden");     
            document.searchForm.EQ_MENU.value = "";
            //The next line does not work in Safari for some reason	    
            //$('qorganism').value = "";
        }
        else {
            utils.removeClass($('orgInfo'), "hidden");
            resetOrganism();
        }
    }
}	

function setDbDefaults(cRadio,setFilters) 
{
    cRadio.checked = true;        
    if(cRadio == $('Rhc') ) {
        if(!$('Ohc').selected) $('Ohc').selected = 'true';         
        //$('fil_l').setAttribute('defVal','unchecked');
        $('blastType').value = 'mapview';        
        $('mixedDb').value = 'on';        
    }
    else if(cRadio == $('Rmc') ) {
        if(!$('Omc').selected) $('Omc').selected = 'true';                
        //$('fil_l').setAttribute('defVal','unchecked');       
        $('blastType').value = 'mapview';        
        $('mixedDb').value = 'on';        
    }
    else {
        if($('Ohc').selected || $('Omc').selected) $('Ogen').selected = 'true';        
        //$('fil_l').setAttribute('defVal','checked');       
        $('blastType').value = '';        
        $('mixedDb').value = "";
    }
    showDbInfoChange();
    if($("bl2seq") && $("bl2seq").checked == true) {
        //Overwrite some settings    
        $('blastType').value = '';        
        $('mixedDb').value = "";        
    }
    
    //Low complexity and repeats
    initFiltersDefaults(setFilters);
    
    diffFromDefault($("DATABASE"));
    //diffFromDefault($("fil_r"));
    //diffFromDefault($("fil_l"));
    DisplayCurrDatabase();     
}
    

function autoSelect(cRadio) {
   utils.addEvent(cRadio, 'click', function() {      
      setDbDefaults(cRadio,true);       
   }, false);
}

function autoCheck(idx) {
   switch (idx) {
       case $('Ohc').index: jQuery($('Rhc')).click(); break;
       case $('Omc').index: jQuery($('Rmc')).click(); break;
       default: jQuery($('Rgen')).click(); break;
   }      
}

function setDbType(idx) {
   var updateFilters = false;
   if($("savedSearch").value != "true") updateFilters = true;
   switch (idx) {
      case $('Ohc').index: setDbDefaults($('Rhc'), updateFilters); break;
      case $('Omc').index: setDbDefaults($('Rmc'),updateFilters);  break;
      default: setDbDefaults($('Rgen'),updateFilters); break;
      
   }      
}




function setDBBasedOpts(repeatsOption,filterVal)
{
    $(repeatsOption).selected = true;
    //$('blastType').value = blastType;
    $('fil_r').checked = filterVal;
}

function setupDynamicFilter() {
    var selectedIndex = getSelectedDbIndex();
    var dbOption = $("DATABASE")[selectedIndex];
    var filterAttr = dbOption.getAttribute("filter");
    if (filterAttr != null) {
        if (filterAttr != "") {//"repeat_9606"
            $(filterAttr).selected = true;
            $("fil_r").checked = true;
        }
        else {
            $("repeat_9606").selected = true;
            $("fil_r").checked = false;
        }
    }
    else {        
        setBlastSpecFilter(true);    
    }
}

function ShowHideCurrDBControls(selDB) {
    var hideAttr = selDB.getAttribute("hide");
    var hide = (hideAttr && hideAttr.indexOf("orgInfo") != -1) ? true : false; //organism	
    hideOrganisms(hide);
    hide = (hideAttr && hideAttr.indexOf("excl") != -1) ? true : false; //exclude boxes
    showHideElem("excl", hide);
    if (hide) {
        if($("exclSeqUncult")) setDefalValue($("exclSeqUncult"));
        if($("exclModels")) setDefalValue($("exclModels"));
    }
    hide = (hideAttr && hideAttr.indexOf("entrezQuery") != -1) ? true : false; //entrez query
    showHideElem("entrezQuery", hide);
    if (hide) {
        setDefalValue($("qquery"));
   }
    
    var hide = (hideAttr && hideAttr.indexOf("orgLbl") != -1) ? true : false; //organism label(to show wgs limit by
    showHideElem("orgLbl", hide);

    var hide = (hideAttr && hideAttr.indexOf("dbHelp") != -1) ? true : false; //organism label(to show help button for database
    showHideElem("dbHelp", hide);
    if (hide) {
        jQuery($("dbHelp")).ncbitoggler("close");
    }

    var hide = (hideAttr && hideAttr.indexOf("seqFTLimitBy") != -1) ? true : false; //seq from type limit
    showHideElem("seqFTLimitBy", hide); //show/hide Seq of Type Limit By block
    if (hide && $("seqFromType")) $("seqFromType").checked = false;   //uncheck Seq of Type include 

    var hide = (hideAttr && hideAttr.indexOf("algAllButBlastp") != -1) ? true : false; //psi,phi,delta blast algorithm
    showHideElem("psiAll", hide); //show/hide psiAll Limit By block    
    showHideElem("phiAll", hide); //show/hide phiAll Limit By block    
    showHideElem("deltaAll", hide); //show/hide deltaAll Limit By block
    if (hide) jQuery("#blastp").click(); //check blastp if hide quick blast
    
     
    var showAttr = selDB.getAttribute("show");
    var show = (showAttr && showAttr.indexOf("wgsLimitBy") != -1) ? true : false; //wgs limit by label
    showHideElem("wgsLimitBy", !show); //show/hide WGS Limit By block        

    var showAttr = selDB.getAttribute("show");
    var show = (showAttr && showAttr.indexOf("shortQ") != -1) ? true : false; //wgs limit by label
    showHideElem("shortQ", !show); //show/hide short query 

    if ($("kmerBlastp")) {
        var show = (showAttr && showAttr.indexOf("kmerAll") != -1) ? true : false; //wgs limit by label
        showHideElem("kmerAll", !show); //show/hide Seq of Type Limit By block
        if (!show && $("kmerBlastp").checked && $("blastp")) jQuery("#blastp").click();    //check blastp if hide quick blast
    } 
}

var resetOrgGlobal;
function AdjustOrgDict(selDB) {
    //Change org dictionary if orgDict attrib is set
    var orgDict = selDB.getAttribute("orgDict");
    if (orgDict && $("qorganism")) {

        var currorgDict = jQuery($($("qorganism"))).ncbiautocomplete("option", "dictionary");
        if (!load && orgDict != currorgDict) {
            resetOrgGlobal = true;
            //alert("Setting resetOrgGlobal=" + resetOrgGlobal);
        }
        jQuery($($("qorganism"))).ncbiautocomplete("option", "dictionary", orgDict);        
        jQuery(".multiOrg").each(function(index) {        
            jQuery($(this)).ncbiautocomplete("option", "dictionary", orgDict);            
        });
    }
}

function SetCurrDBAttrDepndData(selDB) {
    var msgAttr = selDB.getAttribute("msg");
    if (msgAttr && $(msgAttr)) {
        utils.addClass($("upgMsg"), "infoDB");
        utils.addClass($("upgMsg"), "info");
        utils.addClass($("lpgMsg"), "info");
        $("lpgMsg").innerHTML = "<p>" + $(msgAttr).value + "</p>";
    }
    else if (utils.hasClass($("upgMsg"), "infoDB")) {
        utils.removeClass($("upgMsg"), "infoDB");
        utils.removeClass($("upgMsg"), "info");
        utils.removeClass($("lpgMsg"), "info");
        $("lpgMsg").innerHTML = "";
    }
    var sourceDB = selDB.getAttribute("sourceDB");
    if (sourceDB && $("sourceDB")) {
        $("sourceDB").value = sourceDB;
        if ($("sourceDB").value == "sra" && $("dbAbbr")) {
            selDB.setAttribute("abbr", selDB.text);
        }
    }
    if ($('dbAbbr')) {
        var abbrAttr = selDB.getAttribute("abbr");
        $('dbAbbr').value = abbrAttr ? abbrAttr : "";
    }
    //Change org dictionary if orgDict attrib is set
    AdjustOrgDict(selDB);
    
    //Set SHOW_ORGANISM if showOrg attribute
    showOrg = selDB.getAttribute("showOrg");
    if ($("showOrg")) {
        $("showOrg").value = (showOrg) ? showOrg : "";
    }
    dbTaxid = selDB.getAttribute("dbTaxid");
    if ($("dbTaxid")) {
        $("dbTaxid").value = (dbTaxid) ? dbTaxid : "";
    }
    survey = selDB.getAttribute("survey");
    if (survey) {
        blastSurvey();
    }
    
    //Add dictType check here
}


function processOrgDbs() {
    var dbelem = $("DATABASE");

    if (dbelem.type == "select-one") {
        var selectedIndex = getSelectedDbIndex();
        var selDB = $("DATABASE")[selectedIndex];
        var orgDbsAttr = selDB.getAttribute("orgDbs");
                        
        if (orgDbsAttr && $("orgDbs") && ($("qorganism").value != "" || orgDbsAttr.indexOf("giless") != -1 || orgDbsAttr.indexOf("dbvers") != -1)) {
            $("orgDbs").value = orgDbsAttr;
        }
    }
}
 
function DisplayCurrDatabase()
{
	var dbInfo;	
    var dbelem = $("DATABASE");
    var dbName;
    if (dbelem && dbelem.type == "select-one") {        
        var selectedIndex = getSelectedDbIndex();
        dbName = $("DATABASE")[selectedIndex].text;
        var selDB = $("DATABASE")[selectedIndex];
        ShowHideCurrDBControls(selDB);
        SetCurrDBAttrDepndData(selDB);
        //initSRAExtra(selDB);
        //Means standard page - need WGS Db processing
        if ($("wgsLimitBy")) {
            initWGSLimits();
            initOrgFromDbGroup();
        }
    }
    else {
        dbName = $("DATABASE").value;
    }
    jQuery("[class='dbInfo']").each(function(index) {        
        if($("bl2seq") && $("bl2seq").checked == true) {
            dbInfo = $("stype").value + " sequence";
        }
        else if($('dbAbbr') && $('dbAbbr').value != "") {            
            dbInfo = "database " + $('dbAbbr').value;
        }
        else {
            
            dbInfo = "database " + dbName;
            
        }
	    this.innerHTML = dbInfo;	    	
	});
}

function setupDBDisplayParams()
{
    //if($("program").value != "blastn" || $("blastSpec").value != ""){
    if(!HasGenomeNucDatabases()) {
        diffFromDefault($("DATABASE"));
        DisplayCurrDatabase();
        setupDbDynParams();
    }
    else {        
        //Check radiobutton based on selected database
        setDbType($('DATABASE').selectedIndex);               

    }
}

function IsDbIndexed() {
    var selectedIndex = getSelectedDbIndex();
    var dbOption = $("DATABASE")[selectedIndex];
    var dbIndexed = dbOption.getAttribute("dbindex");
    return (dbIndexed && $("selectedProg").value == "megaBlast") ? true : false;
}

function setFiltersDefaults(setFilters, isGenomicDB, repeatsOption) {
    var rVal, lVal;
    var rValDef, lValDef;

    lVal = true;
    lValDef = "checked";
    if (!isGenomicDB) {
        rVal = false;        
        rValDef = "unchecked";        
    }
    else {
        rVal = true;        
        rValDef = "checked";        
    }


    if (setFilters) {
        $('fil_l').checked = lVal;
        $('fil_r').checked = rVal;
        if (repeatsOption != "") $(repeatsOption).selected = true;
    }
    $('fil_l').setAttribute('defVal', lValDef);
    $('fil_r').setAttribute('defVal', rValDef);
    diffFromDefault($("fil_r"));
    diffFromDefault($("fil_l"));
}

function setBlastSpecFilter(setFilters) 
{
    if ($("program").value == "blastn") {
        if ($("blastSpec").value == "OGP__9606__9558") {
            setFiltersDefaults(setFilters, true, "repeat_9606");
        }
        else if ($("blastSpec").value == "OGP__10090__9559") {
            setFiltersDefaults(setFilters, true, "repeat_9989");
        }
    }
}    

function initFiltersDefaults(setFilters) {
    var rVal, lVal;
    var rValDef, lValDef;
    var repeatsOption = "";

    if (HasGenomeNucDatabases()) {
        var isGenomic = true;
        var dbindex = getSelectedDbIndex();        
        if ($('bl2seq') && $('bl2seq').checked) {
            dbindex = $('Ogen').index;
        }
        if (dbindex == $('Ohc').index) {
            repeatsOption = "repeat_9606";
        }
        else if (dbindex == $('Omc').index) {
            repeatsOption = "repeat_9989";
        }
        else {
            isGenomic = false;
            repeatsOption = "repeat_9606";
        }

        setFiltersDefaults(setFilters, isGenomic, repeatsOption);
    }
    else  {
        setBlastSpecFilter(setFilters);
    }
}

    
function initRepeatFilter()
{
    if($("txid") && $("txid").value != "" && $("savedSearch").value != "true") {
        var taxid = $("txid").value;
        var repeatList = $("repeats");
    
        //Check R filter if taxid for the page corresponds to one of the values in the repeat list 
        //and it is not saved search
        for(i=0; i < repeatList.options.length; i++) {
            if(repeatList[i].value.indexOf(taxid) != -1 ||
                        i > 0 && repeatList[i].selected == true) {
                $("fil_r").checked = true;                
                break;
            }
        }
    }
}
  
function bl2SeqInit()
{
    var useBl2seq = this;
    var blastProgram;
    if($("defaultProg") != null) {//This is set for blastn and blastp
        blastProgram = $("defaultProg").value;
    }
    else {       
        blastProgram = $("program").value;            
    }    
    var url = "Blast.cgi?PAGE=" + $("page").value + "&PROGRAM=" + $("program").value + "&BLAST_PROGRAMS=" + blastProgram + "&PAGE_TYPE=BlastSearch";
    
    if(useBl2seq.checked == true) {
        url += "&BLAST_SPEC=blast2seq&DATABASE=n/a";        
    }
    else {
        url+="&DBSEARCH=true";
    }
    if ($("subj") && ($("seq").value.length + $("subj").value.length <= 1500)) {
        url+="&QUERY=" + escape($("seq").value) + "&SUBJECTS=" + escape($("subj").value);
	    if($("QUERY_FROM").value != "") url+="&QUERY_FROM=" + escape($("QUERY_FROM").value);
        if($("QUERY_TO").value != "") url+="&QUERY_TO=" + escape($("QUERY_TO").value);
        if($("SUBJECTS_FROM").value != "") url+="&SUBJECTS_FROM=" + escape($("SUBJECTS_FROM").value);
        if($("SUBJECTS_TO").value != "") url+="&SUBJECTS_TO=" + escape($("SUBJECTS_TO").value);
    }
    location.href = url;    
}        


//Not used now - will be used when stratages download/upload will go in production
function ValidateUserParams()
{
    if(!$("bl2seq") || $("bl2seq").checked == false) {
        ValidateOneUserParam($("DATABASE"),$("USER_DATABASE"),"database");    
    }
    if ($("wordsize")) ValidateOneUserParam($("wordsize"),$("USER_WORD_SIZE"),"wordsize");    
    if ($("matchscores")) ValidateOneUserParam($("matchscores"),$("USER_MATCH_SCORES"),"match/mismatch scores");    
    //TO DO: Add other lists validations
}

//Validates if param supplied by the user is in select box options
function ValidateOneUserParam(listElem, userParamElem,paramName)
{
    var valid = true;
    if (listElem.options && userParamElem && userParamElem.value != "" && 
                                            listElem.selectedIndex && listElem.selectedIndex >= 0) {
        if (listElem[listElem.selectedIndex].value != "") {
            valid = false;
            for (i = 0; i < listElem.options.length; i++) {
                if (listElem[i].value == userParamElem.value) {
                    valid = true;
                    break;
                }
            }
        }
    }     
    if(!valid) {
        alert("Invalid " + paramName + " " + userParamElem.value);
    }
}

function setSeqNum() {
    var selectedIndex = getSelectedDbIndex();
    var dbOption = ($("DATABASE").type == "select-one") ? $("DATABASE")[selectedIndex] : $("DATABASE");
    var dbsList = dbOption.value.split(" ");
    if (dbsList.length == 1) {
        var seqNum = dbOption.getAttribute("seqnum");
        //TO DO:move blasttype here form DisplayCurrDatabase
        //TO DO:Remove enter organism in db list
        //TO DO:Check situation when different databases have different repeat filters
        //Going from R checked to unchecked - code is not written
        if (seqNum && $("seqNum")) {
            $("seqNum").innerHTML = "(" + seqNum + " sequences)";
        }
    }
}

function setupDbDynParams() {
    var selectedIndex = getSelectedDbIndex();
    var dbOption = ($("DATABASE").type == "select-one") ? $("DATABASE")[selectedIndex] : $("DATABASE");

    var blastType = dbOption.getAttribute("blasttype");
    if ($("blastType")) {
        $("blastType").value = (blastType != null) ? blastType : "";
    }
    
    setSeqNum();
    var updateFilters = ($("savedSearch").value != "true") ? true : false;
    setBlastSpecFilter(updateFilters);
} 

function InitLogParams()
{
    this.href += "&LAST_PAGE=" + $("program").value;  
    if($("blastInit")) {
        this.href += "&BLAST_INIT=" + $("blastInit").value;
    }  
    if($("subj") && $("subj").value !="") {
        if($("seq").value.length + $("subj").value.length <=1500) {
            this.href +="&QUERY=" + escape($("seq").value) + "&SUBJECTS=" + escape($("subj").value);
        }
    }
    else {
        if($("seq").value != "" && $("seq").value.length <= 1500) {
            //alert($("seq").value);
            this.href +="&QUERY=" + escape($("seq").value);
        }
    }
    if($("QUERY_FROM").value != "") {
        this.href += "&QUERY_FROM=" + $("QUERY_FROM").value;
    }
    if($("QUERY_TO").value != "") {
        this.href += "&QUERY_TO=" + $("QUERY_TO").value;
    }
    if($("subj")) {
        if($("SUBJECTS_FROM").value != "") {
            this.href += "&SUBJECTS_FROM=" + $("SUBJECTS_FROM").value;
        }
        if($("SUBJECTS_TO").value != "") {
            this.href += "&SUBJECTS_TO=" + $("SUBJECTS_TO").value;
        }
    }       
}

function AddOrgField(e) {
    var orgExclude = !$("qorganism").getAttribute("hideExcl") ? "ORG_EXCLUDE" : "";
    AddOrgRow(e, "EQ_MENU", orgExclude);
    if ($("blastSpec").value.indexOf("MicrobialGenome") != -1 || isSRA() ) {
        initSingleDBStatInfo($("numOrg").value - 1);
    }    
}


function dbGroupInit()
{
    var url = "Blast.cgi?PAGE=" + $("page").value + "&PROGRAM=" + $("program").value + "&BLAST_PROGRAMS=" + $("selectedProg").value + "&PAGE_TYPE=BlastSearch";
    
    //PROGRAM=blastn&BLAST_PROGRAMS=megaBlast&PAGE_TYPE=BlastSearch&BLAST_SPEC=SRA&DB_GROUP=Transcript    
    
    url+="&BLAST_SPEC="  + $("blastSpec").value + "&DB_GROUP=" + this.value;
    
            
    location.href = url;    
}        

function ShowDbSubGroup(selectAll)
{
    var selectedIndex = getSelectedDbIndex();
    var subgroup = $("DATABASE")[selectedIndex].getAttribute("subgroup");    
    if(subgroup) {
        changeOptionList($("subgroup"),subgroup,false);        
        //if($("savedSearch").value != "true") {
        if(selectAll) {
            for(i = 0; i < $("subgroup").options.length; i++) {
                $("subgroup").options[i].selected = true;
            }
        }
        
        if($("sbName")) $("sbName").value = subgroup;
        utils.removeClass($("subgroupDv"), "hidden");        
        utils.removeClass($("subgroupLb"), "hidden");        
    } 
    else{
        $("subgroup").options.length = 0;
        if($("sbName")) $("sbName").value = "";
        utils.addClass($("subgroupDv"), "hidden");        
        utils.addClass($("subgroupLb"), "hidden");
    }
    if ($('dbAbbr')) {
        var abbr = $("DATABASE")[selectedIndex].text.replace(/\(.* entries\)/g, "")
        $('dbAbbr').value = abbr;
    }
}
//subgroup - attribute
function initSubgroup(subgroup) {
    if (subgroup) {        
        jQuery($("subgroup")).html(jQuery($(subgroup)).html());        
        if($("subgroup").selectedIndex == -1) {
            $("subgroup").options[0].selected = true;
        }
        if ($("sbName")) $("sbName").value = subgroup;
        utils.removeClass($("subgroupDv"), "hidden");
        //utils.removeClass($("subgroupLb"), "hidden");
    }
    else {
        if ($("subgroup"))  $("subgroup").options.length = 0;
        if ($("sbName")) $("sbName").value = "";
        if($("subgroupDv")) utils.addClass($("subgroupDv"), "hidden");
        //if($("subgroupLb")) utils.addClass($("subgroupLb"), "hidden");
    }
}

function initDBfromSubGroup(elem) {
    var database = "";
    var dbAbbr = "";
    var hide = "";
    var orgDbs = "";
    var orgDict = "";
    var numSelected = 0;
    for (i = 0; i < $("subgroup").options.length; i++) {        
        var db = $("subgroup").options[i].value;
        var selected = $("subgroup").options[i].selected;
        if (selected) {
            if (dbAbbr != "") dbAbbr += " and ";
            dbAbbr += $("subgroup").options[i].text;
            
            if (orgDict != "") orgDict += "_";
            orgDict += $("subgroup").options[i].getAttribute("orgDict");        
            
            if (database != "") database += " ";
            database += db;
            
            //apply hide to all even if only one selection has hide attribute
            var hideAttr = $("subgroup").options[i].getAttribute("hide");
            if (hideAttr) {
                if (hide != "") hide += " ";
                hide += hideAttr;
            }
            numSelected++;
        }
        if(db == "wgs" && $("orgDbs")) {
            var orgDbsAttr = $("subgroup").options[i].getAttribute("orgDbs");
            //Set hidden field indicating microbial dbs search
            orgDbs = (orgDbsAttr && selected) ? orgDbsAttr : "";
        }        
    }
    if (numSelected == $("subgroup").options.length) dbAbbr = elem.getAttribute("allAbbr");
    if ($(orgDict)) {
        orgDict = $(orgDict).value;
    }
    
    elem.setAttribute("orgDbs", orgDbs);
    elem.setAttribute("database", database);
    elem.setAttribute("abbr", dbAbbr);    
    elem.setAttribute("orgDict", orgDict);
    elem.setAttribute("hide", hide);    
}


//Used for Microbial DBs
function initOrgDobs(displaySubGroup) {
    var elem;
    if (RadioGroup.groups["DB_GROUP"]) {
        elem = RadioGroup.groups["DB_GROUP"].getChecked();        
    }
    else {
        elem = $("DATABASE");
    }
    if (!elem) {
        $("dbGroupRG").checked = true;
        elem = $("dbGroupRG")
    }
    
    if (displaySubGroup) {
        var subgroup = elem.getAttribute("subgroup");
        initSubgroup(subgroup);
    }
    
    var entrez = elem.getAttribute("entrez");
    if (entrez != null && $("entrzPrs")) {
        $("entrzPrs").value = ($("entrzPrs").value == "") ? entrez : entrez + " AND " + $("entrzPrs").value;
    }

    //Set database val for "other_genomic"
    if (elem.id != "DATABASE") {
        if ($("subgroup").options.length > 0) {
            initDBfromSubGroup(elem);
        }
        var dbAttr = elem.getAttribute("database");
        $("DATABASE").value = (dbAttr) ? dbAttr : "";
    }
    //Used only if dictionary changes
    AdjustOrgDict(elem);

    //Set hidden field indicating microbial dbs search
    var orgDbsAttr = elem.getAttribute("orgDbs");
    if ($("orgDbs")) {
        $("orgDbs").value = orgDbsAttr ? orgDbsAttr : "";
        if ($("orgDbs").value != "") {
            if (!utils.hasClass($("orgHint"), "hidden"))  utils.addClass($("orgHint"), "hidden");
        }
        else {
            utils.removeClass($("orgHint"), "hidden");
        }        
    }

    var abbrAttr = elem.getAttribute("abbr");
    if ($('dbAbbr')) {
        $('dbAbbr').value = abbrAttr;
        DisplayCurrDatabase();
    }    
    showDbInfoChange();
    ShowHideCurrDBControls(elem);
}


function setIncludeEntrezQuery(elem) 
{
    var entrez = elem.getAttribute("entrez");
    if (entrez != null && $("entrzPrs")) {        
        if (elem.checked) {
            if ($("entrzPrs").value != "") $("entrzPrs").value += " AND ";            
            $("entrzPrs").value += entrez;
        }        
    }
}

//Used for Microbial DBs
function displayCurrOrg(displaySubGroup) {
    initOrgDobs(displaySubGroup);
    displayDBStatInfo();
}     


function multipleQueries(queryElemID) {
    if ($(queryElemID).value != "") {

        // Defline check
        var seqList;
        var match = /^\s*\>\s*(.[^$]*)$/mi.exec($(queryElemID).value);
        if (match) {
            seqList = match[1].split(">");
        }
        else {
            //accession check
            match = /^\s*([A-Za-z][A-Za-z0-9_.]+[0-9]+)/mi.exec($(queryElemID).value);
            if (!match) {//not accsession
                //gi check
                match = /^\s*[a-z]+\|([0-9]+|[A-Za-z][A-Za-z0-9_.]+[0-9]+)|^\s*(\d+)\b/mi.exec($(queryElemID).value);
            }
            if (match) { //accession or gi
                var trimed = $(queryElemID).value.replace(new RegExp("\\r", "g"), "");
                trimed = trimed.replace(/(^\n*)|(\n*$)/g, "");
                seqList = trimed.split("\n");
                if (seqList.length > 1) {
                    //Take care of this situation: "1 gaattcccgc tacagggggg gcctgaggca ctgcagaaag tgggcctgag cctcgaggat"
                    //find and remove spaces from left and right hand side of string, then check if there is space in between
                    if (seqList[0].replace(/(^\s*)|(\s*$)/g, "").split(" ").length > 1) {
                        return false;
                    }                     
                }
            }
            //otherwise assume one fasta seq without defline
        }
        if (seqList && seqList.length > 1) {
            return true;
        }
    }
    return false;
}

function checkQuerySubrange() {
    if (multipleQueries("seq")) {
        $("QUERY_FROM").value = "";
        $("QUERY_TO").value = "";
        if (this.id != "seq") {
            alert("Sequence sub-range is not supported for multiple sequences");
            $("seq").focus();
            return false;
        }
    }
    return true;
}


function checkSubjSubrange() {
    if (multipleQueries("subj")) {
        $("SUBJECTS_FROM").value = "";
        $("SUBJECTS_TO").value = "";
        if (this.id != "subj") {
            alert("Sequence sub-range is not supported for multiple sequences");
            $("subj").focus();
        }
        return false;
    }
    return true;
}

function showDbInfoChange() {
    if (utils.hasClass($("dbHelp"), "ui-ncbitoggler-open")) {
        showDbDetails();
    }
    else {
        $("dbHelp").setAttribute("getDbInfo", "reset");
    }
}
function showDbInfoInit() {
    if ($("dbHelp").getAttribute("getDbInfo") != "init") {
        showDbDetails();
    }
}

function calcMultiSeqNum() 
{
    var seqNum = 0;
    jQuery("#dbHelpInfo").find("span.sn").each(function (index) {
        seqNum += parseInt(this.innerHTML);
    });
    if (seqNum != 0) {
        if ($("seqNum")) $("seqNum").innerHTML = "(" + seqNum + " sequences)";
    }
}

var g_NumDbRetrieved = 0;
function showDbDetailsOne(dbs, params) {

    if (dbs) {

        var dbInfoUrl =  "getDBInfo.cgi";
        var rp = new RemoteDataProvider(dbInfoUrl);

        rp.onSuccess = function (obj) {
            if (g_MultipleDBS > 1) {
                if ($("dbHelpInfo").innerHTML != "") $("dbHelpInfo").innerHTML += "</br>";
                $("dbHelpInfo").innerHTML += obj.responseText;
                g_NumDbRetrieved++;
                if (g_NumDbRetrieved == g_MultipleDBS) {
                    calcMultiSeqNum();
                }
            }
            else {
                $("dbHelpInfo").innerHTML = obj.responseText;
            }
            $("dbHelp").setAttribute("getDbInfo", "init");
        };
        rp.onError = function(obj) {
            //alert(["error:", this.iActiveRequests, obj.status]);
            $("dbHelpInfo").innerHTML += "error, requests:" + this.iActiveRequests + " status:" + obj.status;
        }
        params = "CMD=getDbInfo&DB_PATH=" + dbs + "&" + params;

        rp.Request(params);
    }
}

var g_MultipleDBS = 0;
function showDbDetails() {

    var dbs, dbBuildName, dbTaxid;
    if ($("DATABASE") && $("DATABASE").type == "select-one") {
        var selectedIndex = getSelectedDbIndex();
        dbs = $("DATABASE")[selectedIndex].value;
        dbBuildName = $("DATABASE")[selectedIndex].getAttribute("dbBuildName");
        dbTaxid = $("DATABASE")[selectedIndex].getAttribute("dbTaxid");
    }
    else if ($("DATABASE") && $("DATABASE").type == "hidden") {
        dbs = $("DATABASE").value;
    }
    if (dbs) {

        var isprot = ($("program").value == "blastp" || $("program").value == "blastx") ? "on" : "off";
        var params = "IS_PROT=" + isprot;

        if ($("dbHyperlinkRef") && $("dbHyperlinkText")) {
            params += "&HYPERLINK_TEXT=" + $("dbHyperlinkText").value + "&HYPERLINK_REF=" + $("dbHyperlinkRef").value;
        }

        var dbPrefix;
        if ($("dbPref") && $("dbPref").value != "") dbPrefix = $("dbPref").value;

        if (dbBuildName) {
            params += "&BUILD_NAME=" + dbBuildName;
        }
        if (dbTaxid) {
            params += "&TAXID=" + dbTaxid;
        }
        if (dbPrefix) {
            params += "&DB_DIR_PREFIX=" + dbPrefix;
        }        
        var isComplete = ($("dbGroupICG") && $("dbGroupICG").checked) ? "off" : "on";
        params += "&IS_COMPLETE=" + isComplete;
        
        var dbsList = dbs.split(" ");
        g_MultipleDBS = dbsList.length;
        if (g_MultipleDBS > 1) $("dbHelpInfo").innerHTML = "";

        for (var i = 0; i < dbsList.length; i++) {
            showDbDetailsOne(dbsList[i], params);
        }

    }
}

function checkBlank() {
    if (this.value == "") {
        displayDBStatInfo();        
    }
}
function initSingleDBStatInfo(numOrg) {
    var id = (numOrg == 0) ? "#qorganism" : "#qorganism" + numOrg;
    var microbGenome = ($("blastSpec").value.indexOf("MicrobialGenome") != -1)? true : false;
    jQuery(id).bind("ncbiautocompletechange", displayDBStatInfo);
    jQuery(id).bind("keyup", checkBlank);
    if (microbGenome) {
        id = (numOrg == 0) ? "#orgExcl" : "#orgExcl" + numOrg;
        jQuery(id).bind("click", displayDBStatInfo);
    }
}

function initDBStatInfo() {
    initSingleDBStatInfo(0);    
    jQuery(".multiOrg").each(function(index) {    
        initSingleDBStatInfo(index + 1);        
    });        
}

//*   USAGE:  getDBInfo.cgi?CMD=getDBTaxStats&EQ_MENU=Bacteria+%28taxid%3A2%29&NUM_ORG=4&EQ_MENU1=Bacillus%2FClostridium+group+%28taxid%3A1239%29&ORG_EXCLUDE1=on&EQ_MENU2=Bacteroidetes%2FChlorobi+group+%28taxid%3A68336%29&ORG_EXCLUDE2=on&EQ_MENU3=Archaeobacteria+%28taxid%3A183925%29&IS_PROT=off&DATABASE=Representative_Chromosomes
function initMicrobStatInfoParams() {
    var params = "CMD=getDBTaxStats";
    var numOrg = 0;
    var db = "";

    if (RadioGroup.groups["DB_GROUP"]) {
        var checkedSet = RadioGroup.groups["DB_GROUP"].getChecked();
        db = checkedSet.getAttribute("database");
    }
    else {
        db = $("DATABASE").value;
    }

    var isProt = ($("program").value == "blastp" || $("program").value == "blastx") ? "on" : "off";
    params += "&IS_PROT=" + isProt + "&DATABASE=" + encodeURIComponent(db);

    if ($("qorganism").value != g_DefaultOrgSuggestMsg) {
        params += "&EQ_MENU=" + $("qorganism").value;
        numOrg++;
    }
    if ($("orgExcl").checked) {
        params += "&ORG_EXCLUDE=on";
    }
    jQuery(".multiOrg").each(function(index) {
        if (this.value != g_DefaultOrgSuggestMsg) {
            params += "&EQ_MENU" + numOrg + "=" + $(this).value;
            var exclId = "orgExcl" + $(this).id.replace("qorganism", "");
            if ($(exclId).checked) {
                params += "&ORG_EXCLUDE" + numOrg + "=on";
            }
            numOrg++;
        }
    });        
    params += "&NUM_ORG=" + numOrg;
    params = (numOrg == 0) ? "" : params;
    return params;
}




//*   USAGE:  getDBInfo.cgi?CMD=getDBTaxStats&EQ_MENU=Bacteria+%28taxid%3A2%29&NUM_ORG=4&EQ_MENU1=Bacillus%2FClostridium+group+%28taxid%3A1239%29&ORG_EXCLUDE1=on&EQ_MENU2=Bacteroidetes%2FChlorobi+group+%28taxid%3A68336%29&ORG_EXCLUDE2=on&EQ_MENU3=Archaeobacteria+%28taxid%3A183925%29&IS_PROT=off&DATABASE=Representative_Chromosomes
function displayDBStatInfo() {
    var params;
    if ($("blastSpec").value.indexOf("MicrobialGenome") != -1) {
        params = initMicrobStatInfoParams();
    }
    else if (($("blastSpec").value == "SRA" && !RadioGroup.groups["DB_GROUP"]) || $("blastType").value == "SRA") {
        params = initSRAStatInfoParams();
    }    
    if(params !="") sendDbStatRequest(params);
}

function sendDbStatRequest(params) {    
    var dbInfoUrl = "getDBInfo.cgi";

    var rp = new RemoteDataProvider(dbInfoUrl);
    var infoElem = $("dbStatInfo") ? $("dbStatInfo") : $("seqNum");

    if (!infoElem) return;

    rp.onSuccess = function(obj) {        
        var replaceMsg = true;
        if (isSRA() && (!$("srxSeqNum") || !$("maxSrxSeqNum"))) {
            //first dbInfo request. Do not replace error message, just add
            replaceMsg = false;
        }            
        infoElem.innerHTML = obj.responseText;
        displayDbInfoMsg(replaceMsg);        
    };

    rp.onError = function(obj) {
        infoElem.innerHTML += "error, requests:" + this.iActiveRequests + " status:" + obj.status;
    }

    rp.Request(params, "POST");
}


function displayDbInfoMsg(replaceMsg) 
{
    if (isSRA()) {
        if ($("srxMsg") && $("srxMsg").value != "") {
            msgType = $("srxMsg").getAttribute("class")        
            utils.addClass($("upgMsg"), msgType);
            utils.addClass($("lpgMsg"), msgType);
            if (replaceMsg) $("lpgMsg").innerHTML = $("srxMsg").value;            
            else $("lpgMsg").innerHTML += $("srxMsg").value;
        }
        else {
            if (replaceMsg) {
                $("lpgMsg").setAttribute("class", "");
                $("lpgMsg").innerHTML = "";
            }
        }
    }
}

function checkSRASearchSetSize() {
    var msg = "";
    var overThreshold = $("srxSeqNum") && $("maxSrxSeqNum") && $("srxSeqNum").value != "" && $("maxSrxSeqNum") != "";
    if (overThreshold) {
        var srxSeqNum = $("srxSeqNum").value.replace(/,/g, "");
        var maxSrxSeqNum = $("maxSrxSeqNum").value.replace(/,/g, "");
        overThreshold = parseInt(srxSeqNum) > parseInt(maxSrxSeqNum);
    }
    if (overThreshold) {
        msg = "Due to resource limitations, BLAST searches of SRA data are limited to " + $("maxSrxSeqNum").value + " reads and your current selection has " + $("srxSeqNum").value + "  reads in it. Please select a smaller/different search set";       
        alert(msg);
    }    
    return overThreshold;
}

//   USAGE:  getDBInfo.cgi?CMD=getSraInfo&EQ_MENU=SRX000016+Ruminococcus+sp.+GM2%2F1+%28taxid%3A451639%29&NUM_ORG=2&EQ_MENU1=SRX000009+Anaerotruncus+colihominis+DSM+17241+%28taxid%3A445972%29
function initSRAStatInfoParams() {
    var params = "CMD=getSraInfo";
    var numOrg = 0;

    var suggestHint = $("suggestHint") ? $("suggestHint").value : $("qorganism").getAttribute("suggestHint");
    if ($("qorganism").value != suggestHint) {
        params += "&EQ_MENU=" + encodeURIComponent($("qorganism").value);
        numOrg++;
    }
    
    jQuery(".multiOrg").each(function(index) {
        if ($(this).value != suggestHint) {
            params += "&EQ_MENU" + numOrg + "=" + encodeURIComponent($(this).value);
            numOrg++;
        }
    });        
    params += "&NUM_ORG=" + numOrg;
    params = (numOrg == 0) ? "" : params;
    return params;
}

function showHideExcludeOrg(hideExcl) 
{
    jQuery(".orgExcl").each(function(index) {    
        if (hideExcl && !utils.hasClass(this, "hidden")) {
            utils.addClass(this, "hidden")
        }
        else if (!hideExcl && utils.hasClass(this, "hidden")) {
            utils.removeClass(this, "hidden")
        }
    });        
}

function initDictSuggestHelp(selElem, defaultOrg) 
{
    var suggestPrompt = $("defaultSgPrompt").value;
    var suggestHelp = $("defaultSgHelp").value;
    if (selElem && !defaultOrg) { //WGS group or SRA database is selected
        var suggestPromptID = selElem.id + "SgPrompt"; //for exmpl: dbGroupWGSBioProjSgPrompt hidden field exists
        if ($(suggestPromptID)) suggestPrompt = $(suggestPromptID).value;
        var suggestHelpID = selElem.id + "SgHelp"
        if ($(suggestHelpID)) suggestHelp = $(suggestHelpID).value;
    }
    jQuery($("suggestPrompt")).html(suggestPrompt);
    jQuery($("suggestHelp")).html(suggestHelp);

    if (resetOrgGlobal && defaultOrg) 
    {                
        //resets organism box to default
        if ($("qorganism").hasAttribute("suggestHint")) $("qorganism").removeAttribute("suggestHint");
        if ($("qorganism").hasAttribute("hideExcl")) $("qorganism").removeAttribute("hideExcl");
         $("qorganism").setAttribute("dictType","taxid");         
        showHideExcludeOrg(false);
    }                
}

function initLimitType(limType)
{
    jQuery("#lblFordbGroupWGSProj").html(limType + " Project");
    g_limitType = limType;
}

var g_limitType;
//Set
function initOrgFromDbGroup() 
{
    var selectedIndex = getSelectedDbIndex();
    var selDB = $("DATABASE")[selectedIndex];        
    orgDict = selDB.getAttribute("orgDict");
    var defaultOrg = (orgDict && orgDict == "taxids_sg") ? true : false;

    var dictType = selDB.getAttribute("dictType");

    var orgDbsAttr = selDB.getAttribute("orgDbs");
    /*
    var limitType;
    if (orgDbsAttr && orgDbsAttr !="" && orgDbsAttr.indexOf("orgDbsOnly_") != -1) {
        limitType = orgDbsAttr.replace("orgDbsOnly_","");//"wgs or tsa                    
    }
    */
    var dynJs = selDB.getAttribute("dynJs");
    if (dynJs) eval(dynJs);

    var selDbGroup = RadioGroup.groups["DB_GROUP"].getChecked();    //WGS
    if (selDbGroup && g_limitType && g_limitType != "WGS") {
        selDbGroup = $(selDbGroup.id.replace("WGS", g_limitType)); //replace WGS on TSA to get access to dbGroupTSA... ids                   
    }
    else if (!selDbGroup && dictType && dictType == "sraexp") { //SRA is selected from the DATABASE menu
        selDbGroup = selDB;
        $('blastType').value = "SRA";
    }
    
    if (selDbGroup) {//selDbGroup is either wgs radiobutton or SRA db selection from dropdown
        suggestHint = selDbGroup.getAttribute("suggestHint");
        if (suggestHint) {
            $("qorganism").setAttribute("suggestHint", suggestHint);
        }
        var hideExcl = selDbGroup.getAttribute("hideExcl");
        if (hideExcl) {
            $("qorganism").setAttribute("hideExcl", hideExcl);
        }
        else {
            $("qorganism").removeAttribute("hideExcl");
        }

        var dictType = selDbGroup.getAttribute("dictType");
        if (dictType) $("qorganism").setAttribute("dictType", dictType);

        var orgDict = selDbGroup.getAttribute("orgDict");
        var currorgDict = jQuery($($("qorganism"))).ncbiautocomplete("option", "dictionary");
        if (orgDict != currorgDict) {
            AdjustOrgDict(selDbGroup);
        }        
        
        showHideExcludeOrg(hideExcl);
    }

    initDictSuggestHelp(selDbGroup, defaultOrg); 
    if (resetOrgGlobal) {        
        if($("seqNum")) $("seqNum").innerHTML = "";        
        resetOrgGlobal = false;
        //alert("Resetting resetOrgGlobal=" + resetOrgGlobal);
        //resets organism box according to dictionary and suggest hint
        $("searchForm").EQ_MENU.value = "";
        resetOrganismControls($("searchForm").EQ_MENU);       
    }
}

function validateWGSInput() 
{
    var valid = true;
    if (RadioGroup.groups["DB_GROUP"]) {
        var wgsDbGroup = RadioGroup.groups["DB_GROUP"].getChecked();
        if (wgsDbGroup && $("qorganism").value == "") {
            var msg = "";
            if (wgsDbGroup.id == "dbGroupWGSOrg") msg = "Please, enter organism under 'Limit by'";
            else if (wgsDbGroup.id == "dbGroupWGSBioProj") msg = "Please, enter  bioproject ID under 'Limit by'";
            else if (wgsDbGroup.id == "dbGroupWGSProj") msg = "Please, enter WGS project under 'Limit by'";
            if (msg != "") {
                valid = false;
                alert(msg);
            }
        }
    }
    return valid;
}

function wgsGroupInit() 
{
    resetOrgGlobal = true;
    initOrgFromDbGroup();            
}


function initWGSLimits() 
{
    var selectedIndex = getSelectedDbIndex();
    var selDB = $("DATABASE")[selectedIndex];
    var showWgsLimits = selDB.getAttribute("show") == "wgsLimitBy";
    //Check Organism Radio button in WGS block if no selection

    var wgsDbGroup = RadioGroup.groups["DB_GROUP"].getChecked();    
    if (showWgsLimits) {
        if(wgsDbGroup) resetOrgGlobal = (load) ? false : true;
        if(!load || !wgsDbGroup) $("dbGroupWGSOrg").checked = true; //select first one
    }
    else if (!load && wgsDbGroup) {
        wgsDbGroup.checked = false;
    }    
}

function getSelectedDbIndex() 
{
    var selectedDbInd = $("DATABASE").selectedIndex;
    selectedDbInd = (selectedDbInd >= 0) ? selectedDbInd : 0;
    return selectedDbInd;
}

if (navigator.userAgent.match(/safari/i)) {
    document.write("<link rel='stylesheet' type='text/css' href='css/safari-blastn.css'  media='screen'/>");
}
if (navigator.userAgent.match(/opera/i)) {
document.write("<link rel='stylesheet' type='text/css' href='css/opera-blastn.css'  media='screen'/>");
}

