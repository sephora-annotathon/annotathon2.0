from django.contrib import admin


# Register your models here.

from django.contrib import admin

from .models import annotathon_tool1, annotathon_tool2, Corrections, Annotathon, Module

admin.site.register(annotathon_tool1)
admin.site.register(annotathon_tool2)
admin.site.register(Corrections)
admin.site.register(Annotathon)
admin.site.register(Module)

