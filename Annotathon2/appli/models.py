from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models import Min, Max

class Line(models.Model):                    # model - class    - table
    text = models.CharField(max_length=255)  # field - instance - row
# Create your models here.

class annotathon_tool1(models.Model):

    User_id = models.CharField(max_length=42)

    Seq_prot = models.CharField(max_length=200)

    Protocole = models.CharField(max_length=42)

    Resultatbrut_Blast = models.TextField(max_length=500)

    Interpretation = models.TextField(max_length=400)

    Commentaire_correcteur = models.TextField(null=True, editable=False)




class annotathon_tool2(models.Model):

    User_id = models.CharField(max_length=42)

    Seq_prot = models.CharField(max_length=200)

    Protocole = models.CharField(max_length=42)

    Resultatbrut_domaines = models.TextField(max_length=500)
    
    Interpretation = models.TextField(max_length=400)

    Commentaire_correcteur = models.TextField(null=True)




class Corrections(models.Model):

    User_id = models.CharField(max_length=42)
  
    first_name = models.CharField(max_length=42, null=False)

    last_name = models.CharField(max_length=42, null=False)

    permission_id = models.IntegerField(null=False)
 
    Seq_prot = models.CharField(max_length=200)

    Com_tool1 = models.TextField(null=True)

    Com_tool2 = models.TextField(null=True)

    Note_comGlobal = models.TextField(null=True)






class Annotathon(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, unique=False,
    )

    # module = models.OneToOneField(Module,
    #    on_delete=models.CASCADE, to_field='nom_du_module'
    # )
  
    Sequence_genomique = models.TextField(max_length=350)
    
    Protocole = models.TextField(max_length=100)
 
    Resultats_Bruts = models.TextField(max_length=1000)
  
    Analyse_Resultats = models.TextField(max_length=400)

    

#class Correction(models.Model):

    #commentaire_protocole = models.OneToOneField(
     #                    Annotations, 
      #                   on_delete=models.CASCADE,
    #)


class Moduleschoice(models.Model):


    ORFinder = models.BooleanField()
    Blast_P = models.BooleanField()
    choix = [('1', 'ORFinder'), ('2', 'Blast P')]

    
class Module(models.Model):
    nom_du_module = models.CharField(max_length=50, unique=True)
    protocole = models.BooleanField(default=True)
    resultats = models.BooleanField(default=True)
    analyses = models.BooleanField(default=True)



class Descriptif_Module(models.Model):
    module = models.OneToOneField(Module,
       on_delete=models.CASCADE, to_field='nom_du_module'
    )

    description = models.TextField(max_length=1000)


class Options(models.Model):
    module_name = models.OneToOneField(Module,
       on_delete=models.CASCADE, to_field='nom_du_module'
    )

    Option_1 = models.CharField(max_length=60)
    Option_2 = models.CharField(max_length=60, blank=True)
    Option_3 = models.CharField(max_length=60, blank=True)
    Option_4 = models.CharField(max_length=60, blank=True)
    Option_5 = models.CharField(max_length=60, blank=True)
    Option_6 = models.CharField(max_length=60, blank=True)



# class mon_module(Moduleschoice):
#     Autre_module = models.OneToOneField(Module,
#        on_delete=models.CASCADE, to_field='nom_du_module'
#     )


class BlastP(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, unique=False,
    )

    # module = models.OneToOneField(Module,
    #    on_delete=models.CASCADE, to_field='nom_du_module'
    # )
  
    Sequence_genomique = models.TextField(max_length=350)
    
    Protocole = models.TextField(max_length=90)
 
    Resultats_Bruts = models.TextField(max_length=1000)
  
    Analyse_Resultats = models.TextField(max_length=400)