from django import forms
from django.contrib.auth.models import User
from .models import BlastP, Annotathon
from .models import Moduleschoice
from .models import Module, Descriptif_Module, Options, Corrections


#eventuellement tester avec modele annotathon_tool1 qui est plus complet...

CHOICES = [('e-value=','< 1.10e-10'), ('e-value=','< 1.10e-60')]



class SeqForm(forms.ModelForm):
    Traduction = forms.CharField(widget=forms.Textarea, label='Traduction', max_length=300)
    Longueur = forms.CharField(label='Longueur', max_length=50)
    Statut = forms.CharField(label='Statut', max_length=30)
    Message = forms.CharField(widget=forms.Textarea, label='Message au correcteur (optionnel)', max_length=300, required=False)
    class Meta:
        model = Annotathon
        fields = ('Sequence_genomique','Protocole','Resultats_Bruts','Analyse_Resultats')
    
    def __init__(self, *args, **kwargs):
        self._user = kwargs.pop('user')
        super(SeqForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        inst = super(SeqForm, self).save(commit=False)
        inst.user = self._user
        if commit:
            inst.save()
            self.save_m2m()
        return inst
    



class BlastForm(forms.ModelForm):
    
    E_value = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CHOICES,)
    Message = forms.CharField(widget=forms.Textarea, label='Message au correcteur (optionnel)', max_length=300, required=False)

    class Meta:
        model = BlastP
        fields = ('Sequence_genomique','Protocole','Resultats_Bruts','Analyse_Resultats')


    def __init__(self, *args, **kwargs):
        self._user = kwargs.pop('user')
        super(BlastForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        inst = super(BlastForm, self).save(commit=False)
        inst.user = self._user
        if commit:
            inst.save()
            self.save_m2m()
        return inst



class UserForm(forms.ModelForm):
    password  = forms.CharField(widget=forms.PasswordInput)
    password_confirm  = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'password_confirm')
    def clean(self):
        cleaned_data = super(UserForm, self).clean()
        password = cleaned_data.get("password")
        password_confirm = cleaned_data.get("password_confirm")

        if password != password_confirm:
            raise forms.ValidationError(
               "Please enter a correct password!"
            )
        return cleaned_data


class ChoiceForm(forms.ModelForm):
    #Modules_for_Annotations = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=Moduleschoice.choix,)
    class Meta:
        model = Moduleschoice
        fields = '__all__'


class Modules(forms.ModelForm):
    class Meta:
        model = Module
        fields = '__all__'

class Description(forms.ModelForm):
    # nom_du_module_à_selectionner = forms.CharField(widget=forms.TextInput(attrs={'placeholder':(Module.objects.order_by('nom_du_module'))}), label='Nom du module à selectionner dans la liste, pour ajouter une description au module ajouté précédemment:',required=False)


    class Meta:
        model = Descriptif_Module
        fields = '__all__'


class Option(forms.ModelForm):

    class Meta:
        model = Options
        fields = '__all__'


class correct(forms.ModelForm):
    class Meta:
        model = Corrections
        fields = ('Commentaire_Sequence', 'commentaire_protocole', 'commntaire_resultats', 'commentaire_analyse', 'Commentaire_global')

    # def __init__(self, *args, **kwargs):
    #     self._user = kwargs.pop('user')
    #     super(SeqForm, self).__init__(*args, **kwargs)

    # def save(self, commit=True):
    #     inst = super(SeqForm, self).save(commit=False)
    #     inst.user = self._user
    #     if commit:
    #         inst.save()
    #         self.save_m2m()
    #     return inst









class Domain(forms.ModelForm):
    Domaines_conservés = forms.IntegerField(label='Nombre de Domaines Conservés') 
    class Meta:
        model = Annotathon
        fields = '__all__'

