from django.shortcuts import render
from django.shortcuts import render_to_response
from django.contrib import messages

from appli.models import Annotathon, BlastP
# Create your views here.

from django.http import HttpResponse
from appli.models import Line
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from django.db.models import F
from .forms import UserForm
from .forms import ChoiceForm, correct
from .forms import Modules, Description, Option, Domain
from .models import Moduleschoice, Descriptif_Module
from .models import Module, Corrections


def index_view(request, connected="True or false"):
    ORFinder = Moduleschoice.objects.values('ORFinder').order_by('-id')[0]
    Blast = Moduleschoice.objects.values('Blast_P').order_by('-id')[0]
    Domaines = Descriptif_Module.objects.values('description').order_by('-id')[0]
    mod = Module.objects.values('protocole').order_by('-id')[0]
    nom = User.objects.values('username').order_by('-id')[0]
    nom5 = User.objects.values('username').order_by('id')[5]
    modna = Module.objects.values('nom_du_module').order_by('-id')[0]
 

    return render(request, "helloDJ/accueil.html",
                                {'modna': modna, 'ORFinder': ORFinder, 'Blast': Blast, 'Domaines' : Domaines, 'mod': mod, 'nom': nom, 'nom5' : nom5, "connected" : request.__dict__['user']})
    


@login_required(login_url='/login/')
def myname_view(request, name="sephora"):
    return HttpResponse("Hello %s!"%name)



def logout_view(request):
    logout(request)
    #return render(request, "helloDJ/logout.html")
    # Redirect to a success page.
    return HttpResponseRedirect('/')



def home(request):
    return HttpResponseRedirect('/')



def new_account(request):
    registered = False
    form = UserForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            password = form.cleaned_data['password']
            user = form.save()
            user.set_password(password)
            user.is_staff = True
            user.save()
            registered = True
        elif form.data['password'] != form.data['password_confirm']:
            form.add_error('password_confirm', 'Be careful when you enter the password!')
        else: 
            print(form.errors)
    else : 
        form = UserForm()

    return render(request, 'helloDJ/newaccount.html', {'form': form, 'registered' : registered, "connected" : request.__dict__['user'] })   
   


def user_registered(request):
    registered = False
    form = UserForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            password = form.cleaned_data['password']
            user = form.save()
            user.set_password(password)
            user.is_staff = True
            user.save()
            registered = True
        elif form.data['password'] != form.data['password_confirm']:
            form.add_error('password_confirm', 'Be careful when you enter the password!')
        else: 
            print(form.errors)
    else : 
        form = UserForm()



    return render(request, 'helloDJ/registered.html', {'form': form, 'registered' : registered})


from .forms import SeqForm, BlastForm

@login_required(login_url='/login/')
def seq_gen(request):
    ORFinder = Moduleschoice.objects.values('ORFinder').order_by('-id')[0]
    Blast = Moduleschoice.objects.values('Blast_P').order_by('-id')[0]
    Domaines = Descriptif_Module.objects.values('description').order_by('-id')[0]
    mod = Module.objects.values('protocole').order_by('-id')[0]
    com1 = Corrections.objects.values('Commentaire_Sequence').order_by('-id')[0]
    com2 = Corrections.objects.values('commentaire_protocole').order_by('-id')[0]
    com3 = Corrections.objects.values('commntaire_resultats').order_by('-id')[0]
    com4 = Corrections.objects.values('commentaire_analyse').order_by('-id')[0]
    com5 = Corrections.objects.values('Commentaire_global').order_by('-id')[0]
    lid = Corrections.objects.values('id').order_by('-id')[0]
    form = SeqForm(request.POST, user=request.user)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':

        # create a form instance and populate it with data from the request :
        annot = form.save()
        #annot.user = User.objects.get(user=self.request.user)
        annot.save()
        if form.is_valid:
            #return HttpResponse("Well done %s!! You can now go back to your annotations."%connected)
            return render(request, 'helloDJ/success.html', {'form': form, "connected": request.__dict__['user']})
    return render(request, 'helloDJ/sequence.html', {'lid' : lid, 'com1' : com1, 'com2': com2, 'com3' : com3, 'com4' : com4, 'com5' : com5, 'form': form, 'ORFinder': ORFinder, 'Blast': Blast, 'Domaines' : Domaines, 'mod': mod, "connected" : request.__dict__['user']})
    messages.add_message(request, messages.INFO, 'Success!!')

    

    # def form_valid(self, form):
    #     candidate = form.save(commit=False)
    #     candidate.user = User.objects.get(user=self.request.user)  # use your own profile here
    #     candidate.save()
    #     return HttpResponseRedirect(self.get_success_url())

# def sample_view(request):
#     current_user = request.user
#     print(current_user.id)



@login_required(login_url='/login/')
def orfinder(request):
     ORFinder = Moduleschoice.objects.values('ORFinder').order_by('-id')[0]
     Blast = Moduleschoice.objects.values('Blast_P').order_by('-id')[0]
     Domaines = Descriptif_Module.objects.values('description').order_by('-id')[0]
     mod = Module.objects.values('protocole').order_by('-id')[0]
     return render(request, 'helloDJ/orfinder.html', {'ORFinder': ORFinder, 'Blast': Blast, 'Domaines' : Domaines, 'mod': mod, "connected" : request.__dict__['user']})
  
@login_required(login_url='/login/')
def blast(request):
    connected = request.__dict__['user']
    ORFinder = Moduleschoice.objects.values('ORFinder').order_by('-id')[0]
    Blast = Moduleschoice.objects.values('Blast_P').order_by('-id')[0]
    Domaines = Descriptif_Module.objects.values('description').order_by('-id')[0]
    mod = Module.objects.values('protocole').order_by('-id')[0]
    form = BlastForm(request.POST, user=request.user)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        annot_blast = form.save()
        annot_blast.save()
        if form.is_valid:
            #return HttpResponse("Well done %s!! You can now go back to your annotations."%connected)
            return render(request, 'helloDJ/success.html', {'form': form, "connected": request.__dict__['user']})
            # redirect to a new URL:
    

    # if a GET (or any other method) we'll create a blank form
    return render(request, 'helloDJ/blast.html', {'form': form, 'ORFinder': ORFinder, 'Blast': Blast, 'Domaines' : Domaines, 'mod': mod, "connected" : request.__dict__['user']})  

@login_required(login_url='/login/')
def domaines(request):
    ORFinder = Moduleschoice.objects.values('ORFinder').order_by('-id')[0]
    Blast = Moduleschoice.objects.values('Blast_P').order_by('-id')[0]
    Domaines = Descriptif_Module.objects.values('description').order_by('-id')[0]
    mod = Module.objects.values('protocole').order_by('-id')[0]
    form=Domain(request.POST)
    if request.method == 'POST':
        dom = form.save()
        dom.save()



    return render(request, 'helloDJ/domaines.html', {'form' : form, 'ORFinder': ORFinder, 'Blast': Blast, 'Domaines' : Domaines, 'mod': mod, "connected" : request.__dict__['user']})


@login_required(login_url='/login/')
def correction(request):
#pour n'avoir que les resultats bruts : Annotations.objects.values('Resultats_Bruts')
    
    liste = []
    for i in range (1,8):
        p1 = User.objects.values('id','username').order_by('id')[i] #attention, ceci récupère les lignes de la table par l'indice et non par l'id. La solution pour récupérer l'id m'est encore inconnue.
        liste.append([p1, "http://127.0.0.1:8000/corrdetail/?id=" + str(i+1)])
    
    return render(request, 'helloDJ/correction.html', {'p1': liste, "connected" : request.__dict__['user']})
    


@login_required(login_url='/login/')
def corrdetail(request):
    for i in range (1,8):
        p1 = User.objects.values('id','username').order_by('id')[i]
    
    key_id = request.GET['id']
    ref = Annotathon.objects.filter(user_id = key_id).values('user_id', 'Sequence_genomique', 'Protocole', 'Resultats_Bruts', 'Analyse_Resultats')[0]
    #bla = BlastP.objects.filter(user_id = key_id).values('user_id', 'Sequence_genomique', 'Protocole', 'Resultats_Bruts', 'Analyse_Resultats')[0]
    if request.user.is_superuser:
        form = correct(request.POST)
    # if this is a POST request we need to process the form data
        if request.method == 'POST':
        # create a form instance and populate it with data from the request:
            corre = form.save()
            corre.save()
        return render(request, 'helloDJ/corrdetails.html', {'key_id': key_id, 'form': form,'ref': ref, 'p1': p1, "connected" : request.__dict__['user']})
    else : 
        return HttpResponseRedirect('/')


@login_required(login_url='/login/')
def your_account(request):

    connected = request.__dict__['user']
    fname = User.objects.filter(username=connected).values('first_name')[0]
    em = User.objects.filter(username=connected).values('email')[0]
    dt = User.objects.filter(username=connected).values('date_joined')[0]
    lname = User.objects.filter(username=connected).values('last_name')[0]
    ORFinder = Moduleschoice.objects.values('ORFinder').order_by('-id')[0]
    Blast = Moduleschoice.objects.values('Blast_P').order_by('-id')[0]
    Domaines = Descriptif_Module.objects.values('description').order_by('-id')[0]
    mod = Module.objects.values('protocole').order_by('-id')[0]
    return render_to_response('helloDJ/youraccount.html', {'ORFinder': ORFinder, 'Blast': Blast, 'Domaines' : Domaines, 'mod': mod, 'us' : request.__dict__['user'], 'fname' : fname, 'em' : em, 'dt' : dt, 'lname' : lname, "connected" : request.__dict__['user']})


def help(request):
    return render(request, 'helloDJ/help.html')


def get_subsequence(request):
    return render(request, 'helloDJ/get_subsequence.html')


def francais(request):
    return render(request, 'helloDJ/français.html', {"connected" : request.__dict__['user']})

@login_required(login_url='/login/')
def choicemodules(request):
    last_module = Module.objects.values('nom_du_module').order_by('-id')[0]
    nom = User.objects.values('username').order_by('id')[7]
    nom5 = User.objects.values('username').order_by('id')[5]

    if request.user.is_superuser:
        form = ChoiceForm(request.POST)
    # if this is a POST request we need to process the form data
        if request.method == 'POST':
        # create a form instance and populate it with data from the request:
            annot_choice = form.save()
            annot_choice.save()
    # tester cette requete pour ne garder que la derniere ligne :
        return render(request, 'helloDJ/moduleschoice.html', {'form' : form, 'last_module' : last_module, 'nom5' : nom5, "connected" : request.__dict__['user']})
    else : 
        return HttpResponseRedirect('/')

@login_required(login_url='/login/')
def modules(request):
    if request.user.is_superuser:
        form = Modules(request.POST)
    #if this is a POST request we need to process the form data
        if request.method == 'POST':

        # create a form instance and populate it with data from the request :
            mod = form.save()
            mod.save()
        return render(request, 'helloDJ/modules.html', {'form': form, "connected": request.__dict__['user']})
    else : 
        return HttpResponseRedirect('/')

@login_required(login_url='/login/')
def descript(request):
    module_name = Module.objects.values('nom_du_module').order_by('-id')[0]
    nom_module = Module.objects.all().order_by('-id')[0]
    nom = User.objects.values('username').order_by('-id')[0]
    nom5 = User.objects.values('username').order_by('id')[5]
    if request.user.is_superuser:
        desc = Description(request.POST)
        if request.method == 'POST':
            des = desc.save()
            des.save()

        return render(request, 'helloDJ/description.html', {'desc': desc, 'module_name' : module_name, 'nom_module' : nom_module, 'nom': nom, 'nom5' : nom5, "connected": request.__dict__['user']})
    else : 
        return HttpResponseRedirect('/')


@login_required(login_url='/login/')
def options(request):
    module_name = Module.objects.values('nom_du_module').order_by('-id')[0]
    nom_module = Module.objects.all().order_by('-id')[0]
    nom = User.objects.values('username').order_by('-id')[0]
    nom5 = User.objects.values('username').order_by('id')[5]
    if request.user.is_superuser:
        opt = Option(request.POST)
        if request.method == 'POST':
            op = opt.save()
            op.save()

        return render(request, 'helloDJ/options.html', {'nom': nom, 'opt' : opt, 'module_name' : module_name, 'nom_module' : nom_module,'nom5' : nom5, "connected": request.__dict__['user']})
    else : 
        return HttpResponseRedirect('/')

# Create your views here.


