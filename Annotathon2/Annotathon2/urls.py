from appli.views import index_view, logout_view, modules
from appli.views import myname_view, new_account, descript
from appli.views import home, user_registered, options
from appli.views import seq_gen, blast
from appli.views import choicemodules
from appli.views import correction, your_account, francais, domaines
from appli.views import help, get_subsequence, corrdetail


urlpatterns = [
    path('', index_view, name='index'),
    path('myname/', myname_view), # protected
    path('login/', auth_views.login),
    path('logout/', logout_view),
    path('new_account/', new_account),
    path('home/', home),
    path('user_registered/', user_registered),
    #path('appli/', include('appli.urls')),
    path('admin/', admin.site.urls),
    path('seq_gen/', seq_gen),
    path('orfinder/', orfinder),
    path('blast/', blast),
    path('correction/', correction),
    path('your_account/', your_account),
    #path('outilblast/', outilblast),
    #path('outilblast/help/', help),
    #path('outilblast/get_subsequence/', get_subsequence),
    #path('orfinder/outilblast/', outilblast),
    path('francais/', francais),
    path('choicemodules/', choicemodules),
    path('modules/', modules),
    path('modules/descript/', descript),
    path('modules/options/', options),
    path('domaines/', domaines),
    path('corrdetail/', corrdetail),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
